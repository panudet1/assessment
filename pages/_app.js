// import '../styles/globals.css'

// function MyApp({ Component, pageProps }) {
//   return <Component {...pageProps} />
// }

// export default MyApp
import "bootstrap/dist/css/bootstrap.min.css";
import "../styles/globals.css";
import Head from "next/head";
function MyApp({ Component, pageProps }) {
  return (
    <>
      <Head>
        <script
          type="text/javascript"
          src="https://cdn.jsdelivr.net/npm/apexcharts"
        ></script>
        <script
          type="text/javascript"
          src="https://cdn.jsdelivr.net/npm/react-apexcharts"
        ></script>
        <script
          async
          defer
          crossOrigin="anonymous"
          src="https://connect.facebook.net/th_TH/sdk.js#xfbml=1&version=v9.0&appId=814874269026743&autoLogAppEvents=1"
          nonce="V6ujGEKX"
        ></script>
      </Head>
      <Component {...pageProps} />
    </>
  );
}
export default MyApp;
