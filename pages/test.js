import React, { useState, useEffect, useRef } from "react";
import { useRouter } from "next/router";
export default function Home() {
  const router = useRouter();
  const myRefdomain1 = useRef(null);
  const myRefdomain2 = useRef(null);
  const myRefdomain3 = useRef(null);
  const myRefdomain4 = useRef(null);
  const myRefdomain5 = useRef(null);
  const myRefdomain6 = useRef(null);
  const myRefdomain7 = useRef(null);
  const myRefdomain8 = useRef(null);
  const myRefdomain9 = useRef(null);
  const myRefdomain10 = useRef(null);
  // var id = await localStorage.getItem("user_id");

  const [domain1, Domain1] = useState([
    { Q: 1, A: "", S: 0 },
    { Q: 2, A: "", S: 0 },
    { Q: 3, A: "", S: 0 },
    { Q: 4, A: "", S: 0 },
    { Q: 5, A: "", S: 0 },
    { Q: 6, A: "", S: 0 },
  ]);
  const [domain2, Domain2] = useState([
    { Q: 1, A: "", S: 0 },
    { Q: 2, A: "", S: 0 },
    { Q: 3, A: "", S: 0 },
    { Q: 4, A: "", S: 0 },
    { Q: 5, A: "", S: 0 },
    { Q: 6, A: "", S: 0 },
  ]);
  const [domain5, Domain5] = useState([
    { Q: 1, A: "", S: 0 },
    { Q: 2, A: "", S: 0 },
    { Q: 3, A: "", S: 0 },
    { Q: 4, A: "", S: 0 },
    { Q: 5, A: "", S: 0 },
    { Q: 6, A: "", S: 0 },
  ]);
  const [domain8, Domain8] = useState([
    { Q: 1, A: "", S: 0 },
    { Q: 2, A: "", S: 0 },
    { Q: 3, A: "", S: 0 },
    { Q: 4, A: "", S: 0 },
    { Q: 5, A: "", S: 0 },
    { Q: 6, A: "", S: 0 },
  ]);
  const [domain3, Domain3] = useState([
    { Q: 1, A: "" },
    { Q: 2, A: "" },
    { Q: 3, A: "" },
    { Q: 4, A: "" },
    { Q: 5, A: "" },
    { Q: 6, A: "" },
  ]);
  const [domain4, Domain4] = useState([
    { Q: 1, A: "" },
    { Q: 2, A: "" },
    { Q: 3, A: "" },
    { Q: 4, A: "" },
    { Q: 5, A: "" },
    { Q: 6, A: "" },
  ]);
  const [domain6, Domain6] = useState([
    { Q: 1, A: "" },
    { Q: 2, A: "" },
    { Q: 3, A: "" },
    { Q: 4, A: "" },
    { Q: 5, A: "" },
    { Q: 6, A: "" },
  ]);
  const [domain7, Domain7] = useState([
    { Q: 1, A: "" },
    { Q: 2, A: "" },
    { Q: 3, A: "" },
    { Q: 4, A: "" },
    { Q: 5, A: "" },
    { Q: 6, A: "" },
  ]);

  const [domain9, Domain9] = useState([
    { Q: 1, A: "", D: "" },
    { Q: 2, A: "", D: "" },
    { Q: 3, A: "", D: "" },
    { Q: 4, A: "", D: "" },
    { Q: 5, A: "", D: "" },
    { Q: 6, A: "", D: "" },
  ]);
  const [domain10, Domain10] = useState([
    { Q: 1, A: "", D: "" },
    { Q: 2, A: "", D: "" },
    { Q: 3, A: "", D: "" },
    { Q: 4, A: "", D: "" },
  ]);
  const ChangeDomain1 = async (q, a, s) => {
    let myArray = domain1;
    var objIndex = myArray.findIndex((obj) => obj.Q == q);
    myArray[objIndex].A = a;
    myArray[objIndex].S = s;
    Domain1(myArray);
  };
  const ChangeDomain2 = async (q, a, s) => {
    let myArray = domain2;
    var objIndex = myArray.findIndex((obj) => obj.Q == q);
    myArray[objIndex].A = a;
    myArray[objIndex].S = s;
    Domain2(myArray);
  };
  const ChangeDomain5 = async (q, a, s) => {
    let myArray = domain5;
    var objIndex = myArray.findIndex((obj) => obj.Q == q);
    myArray[objIndex].A = a;
    myArray[objIndex].S = s;
    Domain5(myArray);
  };
  const ChangeDomain8 = async (q, a, s) => {
    let myArray = domain8;
    var objIndex = myArray.findIndex((obj) => obj.Q == q);
    myArray[objIndex].A = a;
    myArray[objIndex].S = s;
    Domain8(myArray);
  };
  const ChangeDomain3 = async (q, a, s) => {
    let myArray = domain3;
    var objIndex = myArray.findIndex((obj) => obj.Q == q);
    myArray[objIndex].A = a;
    myArray[objIndex].S = a;
    Domain3(myArray);
  };

  const ChangeDomain4 = async (q, a) => {
    let myArray = domain4;
    var objIndex = myArray.findIndex((obj) => obj.Q == q);
    myArray[objIndex].A = a;
    Domain4(myArray);
  };
  const ChangeDomain6 = async (q, a) => {
    let myArray = domain6;
    var objIndex = myArray.findIndex((obj) => obj.Q == q);
    myArray[objIndex].A = a;
    Domain6(myArray);
  };
  const ChangeDomain7 = async (q, a) => {
    let myArray = domain7;
    var objIndex = myArray.findIndex((obj) => obj.Q == q);
    myArray[objIndex].A = a;
    Domain7(myArray);
  };

  const ChangeDomain9 = async (q, a, d) => {
    let myArray = domain9;
    var objIndex = myArray.findIndex((obj) => obj.Q == q);
    myArray[objIndex].A = a;
    myArray[objIndex].D = d;
    Domain9(myArray);
  };
  const ChangeDomain10 = async (q, a, d) => {
    let myArray = domain10;
    var objIndex = myArray.findIndex((obj) => obj.Q == q);
    myArray[objIndex].A = a;
    myArray[objIndex].D = d;
    Domain10(myArray);
  };

  const executeScroll = (DomainName) => {
    var Ref = DomainName;
    Ref.current.scrollIntoView();
  };

  const SendAnswer = async () => {
    for (var i = 0; i < domain1.length; i++) {
      if (domain1[i].A === "") {
        executeScroll(myRefdomain1);
        return false;
      }
    }
    for (var i = 0; i < domain2.length; i++) {
      if (domain2[i].A === "") {
        executeScroll(myRefdomain2);
        return false;
      }
    }
    for (var i = 0; i < domain3.length; i++) {
      if (domain3[i].A === "") {
        executeScroll(myRefdomain3);
        return false;
      }
    }
    for (var i = 0; i < domain4.length; i++) {
      if (domain4[i].A === "") {
        executeScroll(myRefdomain4);
        return false;
      }
    }
    for (var i = 0; i < domain5.length; i++) {
      if (domain5[i].A === "") {
        executeScroll(myRefdomain5);
        return false;
      }
    }
    for (var i = 0; i < domain6.length; i++) {
      if (domain6[i].A === "") {
        executeScroll(myRefdomain6);
        return false;
      }
    }
    for (var i = 0; i < domain7.length; i++) {
      if (domain7[i].A === "") {
        executeScroll(myRefdomain7);
        return false;
      }
    }
    for (var i = 0; i < domain8.length; i++) {
      if (domain8[i].A === "") {
        executeScroll(myRefdomain8);
        return false;
      }
    }
    for (var i = 0; i < domain9.length; i++) {
      if (domain9[i].A === "") {
        executeScroll(myRefdomain9);
        return false;
      }
    }
    for (var i = 0; i < domain10.length; i++) {
      if (domain10[i].A === "") {
        executeScroll(myRefdomain10);
        return false;
      }
    }

    // series_multiple
    var score_logic = 0;
    domain1.forEach((element) => (score_logic = score_logic + element.S));
    var score_giving = 0;
    domain2.forEach((element) => (score_giving = score_giving + element.S));
    var score_competency = 0;
    domain5.forEach(
      (element) => (score_competency = score_competency + element.S)
    );
    var score_integrity = 0;
    domain8.forEach(
      (element) => (score_integrity = score_integrity + element.S)
    );
    var series_multiple = [
      {
        name: "Series 1",
        data: [score_logic, score_giving, score_competency, score_competency],
      },
    ];

    // series_multiple
    var score_teamwork = 0;
    domain3.forEach((element) => (score_teamwork = score_teamwork + element.A));
    var score_communication = 0;
    domain4.forEach(
      (element) => (score_communication = score_communication + element.A)
    );
    var score_passion = 0;
    domain6.forEach((element) => (score_passion = score_passion + element.A));
    var score_grit = 0;
    domain7.forEach((element) => (score_grit = score_grit + element.A));
    var series_scale_likert = [
      {
        name: "Series 1",
        data: [score_teamwork, score_communication, score_passion, score_grit],
      },
    ];
    // extra points
    var ambition = 0;
    var creativity = 0;
    var social = 0;
    var giver = 0;

    domain9.forEach(function (element) {
      if (element.D == "a") {
        ambition = ambition + 1;
      }
      if (element.D == "c") {
        creativity = creativity + 1;
      }
      if (element.D == "s") {
        social = social + 1;
      }
      if (element.D == "g") {
        giver = giver + 1;
      }
    });

    var series_multiple_extra = [
      {
        name: "Series 1",
        data: [ambition, creativity, social, giver],
      },
    ];

    // extra ocean
    var o = 0;
    var c = 0;
    var e = 0;
    var a = 0;
    var n = 0;
    domain10.forEach(function (element) {
      if (element.D == "O") {
        o = o + 1;
      }
      if (element.D == "C") {
        c = c + 1;
      }
      if (element.D == "E") {
        e = e + 1;
      }
      if (element.D == "A") {
        a = a + 1;
      }
      if (element.D == "N") {
        n = n + 1;
      }
    });
    var series_multiple_ocean = [
      {
        name: "Series 1",
        data: [o, c, e, a, n],
      },
    ];
    var user_id = await localStorage.getItem("user_id");
    var data = {
      user_id: user_id,
      domain1: domain1,
      domain2: domain2,
      domain3: domain3,
      domain4: domain4,
      domain5: domain5,
      domain6: domain6,
      domain7: domain7,
      domain8: domain8,
      domain9: domain9,
      domain10: domain10,
      series_multiple: series_multiple,
      series_scale_likert: series_scale_likert,
      series_multiple_extra: series_multiple_extra,
      series_multiple_ocean: series_multiple_ocean,
    };
    fetch("/api/Apply", {
      method: "POST", // or 'PUT'
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer my-token",
        "My-Custom-Header": "foobar",
      },
      body: JSON.stringify(data),
    })
      .then((response) => response.json())
      .then((data) => {
        // console.log(data.data);
        localStorage.setItem("id", data.data);
        return router.push("/chart");
      })

      .catch((error) => {
        console.error("Error:", error);
      });
  };

  return (
    <>
      <div className="container mt-5 mb-5">
        <h4>
          <div ref={myRefdomain1}>
            <h4 className="mt-5">โดเมนที่ 1</h4>
          </div>
          <div className="lopende-tekst vraag rij-even" id="vraag-0-1">
            <h4>
              <span>1.</span>
            </h4>
            <div className="subvraag horizontaal">
              <p>
                <img
                  src="items/item1/uitgevouwen.png"
                  style={{ height: 150 }}
                />
              </p>
              <h4>
                ลูกเต๋าในรูปใดต่อไปนี้ไม่สามารถคลี่ออกมาเป็นภาพด้านบนได้ ?
              </h4>
              <p>
                <label htmlFor="vraag-0-0-1">
                  <input
                    type="radio"
                    name="q[1]"
                    defaultValue={1}
                    id="vraag-0-0-1"
                    onClick={() => ChangeDomain1(1, 1, 0)}
                  />
                  <img src="items/item1/goed1.png" style={{ height: 100 }} />
                </label>
                <label htmlFor="vraag-0-0-4">
                  <input
                    type="radio"
                    name="q[1]"
                    defaultValue={4}
                    id="vraag-0-0-4"
                    onClick={() => ChangeDomain1(1, 2, 1)}
                  />
                  <img src="items/item1/fout.png" style={{ height: 100 }} />
                </label>
                <label htmlFor="vraag-0-0-2">
                  <input
                    type="radio"
                    name="q[1]"
                    defaultValue={2}
                    id="vraag-0-0-2"
                    onClick={() => ChangeDomain1(1, 3, 0)}
                  />
                  <img src="items/item1/goed2.png" style={{ height: 100 }} />
                </label>
                <label htmlFor="vraag-0-0-3">
                  <input
                    type="radio"
                    name="q[1]"
                    defaultValue={3}
                    id="vraag-0-0-3"
                    onClick={() => ChangeDomain1(1, 4, 0)}
                  />
                  <img src="items/item1/goed3.png" style={{ height: 100 }} />
                </label>
              </p>
            </div>
          </div>
          <div className="lopende-tekst vraag rij-oneven" id="vraag-0-2">
            <h4>
              <span>2.</span>
            </h4>
            <div className="subvraag horizontaal">
              <p>
                <img
                  src="items/item2/uitgevouwen.png"
                  style={{ height: 150 }}
                />
              </p>

              <h4>
                ลูกเต๋าในรูปใดต่อไปนี้ไม่สามารถคลี่ออกมาเป็นภาพด้านบนได้ ?
              </h4>

              <p>
                <label htmlFor="vraag-0-1-1">
                  <input
                    type="radio"
                    name="q[2]"
                    defaultValue={1}
                    id="vraag-0-1-1"
                    onClick={() => ChangeDomain1(2, 1, 0)}
                  />
                  <img src="items/item2/goed1.png" style={{ height: 100 }} />
                </label>
                <label htmlFor="vraag-0-1-4">
                  <input
                    type="radio"
                    name="q[2]"
                    defaultValue={4}
                    id="vraag-0-1-4"
                    onClick={() => ChangeDomain1(2, 2, 1)}
                  />
                  <img src="items/item2/fout.png" style={{ height: 100 }} />
                </label>
                <label htmlFor="vraag-0-1-2">
                  <input
                    type="radio"
                    name="q[2]"
                    defaultValue={2}
                    id="vraag-0-1-2"
                    onClick={() => ChangeDomain1(2, 3, 0)}
                  />
                  <img src="items/item2/goed2.png" style={{ height: 100 }} />
                </label>
                <label htmlFor="vraag-0-1-3">
                  <input
                    type="radio"
                    name="q[2]"
                    defaultValue={3}
                    id="vraag-0-1-3"
                    onClick={() => ChangeDomain1(2, 4, 0)}
                  />
                  <img src="items/item2/goed3.png" style={{ height: 100 }} />
                </label>
              </p>
            </div>
          </div>
          <div className="lopende-tekst vraag rij-even" id="vraag-0-3">
            <h4>
              <span>3.</span>
            </h4>
            <div className="subvraag horizontaal">
              <p>
                <img src="items/item3/item.svg" style={{ height: 150 }} />
              </p>

              <h4>รูปใดต่อไปนี้ที่เป็นรูปที่หมุนจากรูปด้านบน ?</h4>

              <p>
                <label htmlFor="vraag-0-2-1">
                  <input
                    type="radio"
                    name="q[3]"
                    defaultValue={1}
                    id="vraag-0-2-1"
                    onClick={() => ChangeDomain1(3, 1, 0)}
                  />
                  <img src="items/item3/item-4.svg" style={{ height: 100 }} />
                </label>
                <label htmlFor="vraag-0-2-2">
                  <input
                    type="radio"
                    name="q[3]"
                    defaultValue={2}
                    id="vraag-0-2-2"
                    onClick={() => ChangeDomain1(3, 2, 0)}
                  />
                  <img src="items/item3/item-2.svg" style={{ height: 100 }} />
                </label>
                <label htmlFor="vraag-0-2-3">
                  <input
                    type="radio"
                    name="q[3]"
                    defaultValue={3}
                    id="vraag-0-2-3"
                    onClick={() => ChangeDomain1(3, 3, 0)}
                  />
                  <img src="items/item3/item-3.svg" style={{ height: 100 }} />
                </label>
                <label htmlFor="vraag-0-2-4">
                  <input
                    type="radio"
                    name="q[3]"
                    defaultValue={4}
                    id="vraag-0-2-4"
                    onClick={() => ChangeDomain1(3, 4, 1)}
                  />
                  <img src="items/item3/item-1.svg" style={{ height: 100 }} />
                </label>
              </p>
            </div>
          </div>
          <div className="lopende-tekst vraag rij-oneven" id="vraag-0-4">
            <h4>
              <span>4.</span>
            </h4>
            <div className="subvraag horizontaal">
              <p>
                <img src="items/item4/item.svg" style={{ height: 150 }} />
              </p>

              <h4>รูปใดต่อไปนี้ที่เป็นรูปที่หมุนจากรูปด้านบน ?</h4>
              <p>
                <label htmlFor="vraag-0-3-1">
                  <input
                    type="radio"
                    name="q[4]"
                    defaultValue={1}
                    id="vraag-0-3-1"
                    onClick={() => ChangeDomain1(4, 1, 1)}
                  />
                  <img src="items/item4/item-1.svg" style={{ height: 100 }} />
                </label>
                <label htmlFor="vraag-0-3-2">
                  <input
                    type="radio"
                    name="q[4]"
                    defaultValue={2}
                    id="vraag-0-3-2"
                    onClick={() => ChangeDomain1(4, 2, 0)}
                  />
                  <img src="items/item4/item-2.svg" style={{ height: 100 }} />
                </label>
                <label htmlFor="vraag-0-3-3">
                  <input
                    type="radio"
                    name="q[4]"
                    defaultValue={3}
                    id="vraag-0-3-3"
                    onClick={() => ChangeDomain1(4, 3, 0)}
                  />
                  <img src="items/item4/item-3.svg" style={{ height: 100 }} />
                </label>
                <label htmlFor="vraag-0-3-4">
                  <input
                    type="radio"
                    name="q[4]"
                    defaultValue={4}
                    id="vraag-0-3-4"
                    onClick={() => ChangeDomain1(4, 4, 0)}
                  />
                  <img src="items/item4/item-4.svg" style={{ height: 100 }} />
                </label>
              </p>
            </div>
          </div>
          <div className="lopende-tekst vraag rij-oneven" id="vraag-0-6">
            <h4>
              <span>5.</span>
            </h4>
            <div className="subvraag horizontaal">
              <p>
                <img src="items/item6/item.svg" style={{ height: 150 }} />
              </p>
              <h4>รูปใดต่อเป็นนี้เป็นรูปมุมสูงของรูปข้างบน ?</h4>
              <p>
                <label htmlFor="vraag-0-5-1">
                  <input
                    type="radio"
                    name="q[6]"
                    defaultValue={1}
                    id="vraag-0-5-1"
                    onClick={() => ChangeDomain1(5, 1, 0)}
                  />
                  <img src="items/item6/item-2.svg" style={{ height: 100 }} />
                </label>
                <label htmlFor="vraag-0-5-2">
                  <input
                    type="radio"
                    name="q[6]"
                    defaultValue={2}
                    id="vraag-0-5-2"
                    onClick={() => ChangeDomain1(5, 2, 1)}
                  />
                  <img src="items/item6/item-1.svg" style={{ height: 100 }} />
                </label>
                <label htmlFor="vraag-0-5-3">
                  <input
                    type="radio"
                    name="q[6]"
                    defaultValue={3}
                    id="vraag-0-5-3"
                    onClick={() => ChangeDomain1(5, 3, 0)}
                  />
                  <img src="items/item6/item-3.svg" style={{ height: 100 }} />
                </label>
                <label htmlFor="vraag-0-5-4">
                  <input
                    type="radio"
                    name="q[6]"
                    defaultValue={4}
                    id="vraag-0-5-4"
                    onClick={() => ChangeDomain1(5, 4, 0)}
                  />
                  <img src="items/item6/item-4.svg" style={{ height: 100 }} />
                </label>
              </p>
            </div>
          </div>
          <div className="lopende-tekst vraag rij-even" id="vraag-0-7">
            <h4>
              <span>6.</span>
            </h4>
            <div className="subvraag horizontaal">
              <p>
                <img src="items/item7/item.svg" style={{ height: 150 }} />
              </p>

              <h4>
                รูปใดต่อไปนี้สามารถนำมาประกบกับรูปที่ให้แล้วสามารถเป็นลูกบาศก์ได้
                ?
              </h4>

              <p>
                <label htmlFor="vraag-0-6-1">
                  <input
                    type="radio"
                    name="q[7]"
                    defaultValue={1}
                    id="vraag-0-6-1"
                    onClick={() => ChangeDomain1(6, 1, 1)}
                  />
                  <img src="items/item7/item-1.svg" style={{ height: 100 }} />
                </label>
                <label htmlFor="vraag-0-6-2">
                  <input
                    type="radio"
                    name="q[7]"
                    defaultValue={2}
                    id="vraag-0-6-2"
                    onClick={() => ChangeDomain1(6, 2, 0)}
                  />
                  <img src="items/item7/item-2.svg" style={{ height: 100 }} />
                </label>
                <label htmlFor="vraag-0-6-3">
                  <input
                    type="radio"
                    name="q[7]"
                    defaultValue={3}
                    id="vraag-0-6-3"
                    onClick={() => ChangeDomain1(6, 3, 0)}
                  />
                  <img src="items/item7/item-3.svg" style={{ height: 100 }} />
                </label>
                <label htmlFor="vraag-0-6-4">
                  <input
                    type="radio"
                    name="q[7]"
                    defaultValue={4}
                    id="vraag-0-6-4"
                    onClick={() => ChangeDomain1(6, 4, 0)}
                  />
                  <img src="items/item7/item-4.svg" style={{ height: 100 }} />
                </label>
              </p>
            </div>
          </div>
          <div ref={myRefdomain2}>
            <h4 className="mt-5">โดเมนที่ 2</h4>
          </div>
          <div>
            <p>
              1. เมื่อถึงคืนวันศุกร์ คุณตั้งหน้าตั้งตารอคืนนี้เพื่อที่จะอยู่บ้าน
              ดูทีวีอย่างเงียบสงบ ในขณะที่กำลังผ่อนคลายอยู่บนโซฟา
              เพื่อนสนิทของคุณมาบ้านและประกาศว่าเธออยากจะออกไปทานอาหารค่ำที่ร้านอาหารและปาร์ตี้ต่อ
              คุณจะทำอย่างไร?
            </p>
            <div className="form-check ml-5">
              <input
                type="radio"
                name="D2[1]"
                className="form-check-input"
                onClick={() => ChangeDomain2(1, 1, 1)}
              />
              <label className="form-check-label">
                a) บอกว่าจริงๆ แล้วตั้งใจจะอยู่บ้าน แต่ก็ยอมออกไปด้วย
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D2[1]"
                className="form-check-input"
                onClick={() => ChangeDomain2(1, 2, 0)}
              />
              <label className="form-check-label">
                b) ปฎิเสธข้อเสนอของเพื่อนด้วยความรู้สึกผิด
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D2[1]"
                className="form-check-input"
                onClick={() => ChangeDomain2(1, 3, 0)}
              />
              <label className="form-check-label">
                c) บอกว่าวันนี้ขอผ่านก่อน เดี๋ยวไปด้วยวันอื่นแทน
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D2[1]"
                className="form-check-input"
                onClick={() => ChangeDomain2(1, 4, 0)}
              />
              <label className="form-check-label">
                d) ปฏิเสธที่จะไป แนะนำให้โทรหาเพื่อนคนอื่นและไปกับคนนั้นแทน
              </label>
            </div>
          </div>
          <div>
            <p>
              2. คุณกำลังนั่งทำงานเอกสารสำคัญอยู่บนโซฟา
              น้องชาย/น้องสาวตัวน้อยของคุณ
              เดินเข้ามาเปิดทีวีดูการ์ตูนและนั่งลงข้างๆ คุณ
              คุณแทบจะไม่มีสมาธิกับงานที่ทำอยู่ คุณจะทำอย่างไร
            </p>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D2[2]"
                className="form-check-input"
                onClick={() => ChangeDomain2(2, 1, 0)}
              />
              <label className="form-check-label">
                a) ขอให้น้องชาย/น้องสาวปิดทีวีไว้ก่อนจนคุณจะทำงานเสร็จ
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D2[2]"
                className="form-check-input"
                onClick={() => ChangeDomain2(2, 2, 0)}
              />
              <label className="form-check-label">
                b) เอื้อมไปหยิบรีโมทแล้วถามว่าขอเบาเสียงลงหน่อยได้ไหม
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D2[2]"
                className="form-check-input"
                onClick={() => ChangeDomain2(2, 3, 1)}
              />
              <label className="form-check-label">
                c) คุณทำงานต่อไปและพยายามไม่สนใจเสียงทีวี
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D2[2]"
                className="form-check-input"
                onClick={() => ChangeDomain2(2, 4, 0)}
              />
              <label className="form-check-label">
                d) คุณไปทำงานที่ห้องอื่นเพื่อที่จะได้ทำงานโดยไม่ถูกรบกวน
              </label>
            </div>
          </div>
          <div>
            <p>
              3. คุณเพิ่งชนะชิงโชคทริปท่องเที่ยวแบบไม่เสียค่าใช้จ่าย
              น่าเสียดายที่ครอบครัวของคุณเพิ่งเริ่มโปรเจคงานใหม่และไม่สามารถมีเวลาว่างไปกับคุณได้
              นี่เป็นโอกาสที่พวกเขาจะรู้สึกเสียดายจริงๆ ถ้าไม่ได้ไป
              คุณจะทำอย่างไร?
            </p>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D2[3]"
                className="form-check-input"
                onClick={() => ChangeDomain2(3, 1, 0)}
              />
              <label className="form-check-label">
                a) เก็บกระเป๋าเดินทาง เพราะคุณไม่อยากจะทิ้งทริปนี้อย่างเสียเปล่า
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D2[3]"
                className="form-check-input"
                onClick={() => ChangeDomain2(3, 2, 0)}
              />
              <label className="form-check-label">
                b) บอกครอบครัวว่าเข้าใจว่ามันคงไม่สนุกเท่าที่ไปด้วยกัน
                แต่ก็ตัดสินใจไปเพราะโอกาสแบบนี้ไม่ได้เกิดขึ้นบ่อยๆ
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D2[3]"
                className="form-check-input"
                onClick={() => ChangeDomain2(3, 3, 0)}
              />
              <label className="form-check-label">
                c) ตัดสินใจที่จะไม่ไป และโพสขายทริปนี้ในราคาถูกให้คนอื่นต่อบน
                facebook
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D2[3]"
                className="form-check-input"
                onClick={() => ChangeDomain2(3, 4, 1)}
              />
              <label className="form-check-label">
                d) คุณจะคุยเรื่องนี้กับครอบครัวและในที่สุดก็ตัดสินใจที่จะไม่ไป -
                เพราะคุณคงรู้สึกแย่ที่จะไปในขณะที่ต้องปล่อยให้พวกเขาต้องทำงานอยู่
              </label>
            </div>
          </div>
          <div>
            <p>
              4.
              คุณสมัครงานในตำแหน่งผู้จัดการแะหัวหน้างานเก่าของคุณเขียนจดหมายแนะนำคุณอย่างดี
              คุณจะ…
            </p>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D2[4]"
                className="form-check-input"
                onClick={() => ChangeDomain2(4, 1, 0)}
              />
              <label className="form-check-label">
                a) หาทางตอบแทนเจ้านายเก่าเพราะบุญคุณต้องทดแทน
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D2[4]"
                className="form-check-input"
                onClick={() => ChangeDomain2(4, 2, 1)}
              />
              <label className="form-check-label">
                b) เสนอว่าจะช่วยประชาสัมพันธ์ให้บริษัท
                เพื่อที่จะให้มีลูกจ้างคนอื่นมาสมัครงานแทนในตำแหน่งที่เพิ่งลาออกมา
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D2[4]"
                className="form-check-input"
                onClick={() => ChangeDomain2(4, 3, 0)}
              />
              <label className="form-check-label">
                c)
                มุ่งหน้าสร้างความประทับใจให้เจ้านายใหม่เผื่อว่าถ้าหากออกจากงานนี้ไปในอนาคตก็จะได้จดหมายแนะนำดีๆ
                อีก
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D2[4]"
                className="form-check-input"
                onClick={() => ChangeDomain2(4, 4, 0)}
              />
              <label className="form-check-label">
                d) พยายามทำงานให้เต็มความสามารถเพื่อไม่ให้เจ้านายเก่าเสียหน้า
              </label>
            </div>
          </div>
          <div>
            <p>
              5. เพื่อนร่วมงานใหม่ของคุณกำลังมองหางานให้สามีของเธอ
              โดยเธอถามคุณว่าคุณรู้จักใครสักคนในบริษัท Kramerica ไหม
              คุณตอบว่ารู้จักซึ่งวันถัดไปคุณนึกได้ว่าคุณรู้จักกับบริษัทอื่นที่สายงานคล้ายกับบริษัทนี้
              3 บริษัท คุณจะ…
            </p>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D2[5]"
                className="form-check-input"
                onClick={() => ChangeDomain2(5, 1, 1)}
              />
              <label className="form-check-label">
                a) แนะนำสามีของเพื่อนร่วมงานให้รู้จักกับคนในบริษัททั้ง 4 แห่ง
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D2[5]"
                className="form-check-input"
                onClick={() => ChangeDomain2(5, 2, 0)}
              />
              <label className="form-check-label">
                b)
                ดูว่าสามีของเพื่อนร่วมงานน่าจะเหมาะกับบริษัทไหนแล้วค่อยแนะนำให้รู้จักบริษัทนั้น
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D2[5]"
                className="form-check-input"
                onClick={() => ChangeDomain2(5, 3, 0)}
              />
              <label className="form-check-label">
                c) แนะนำสามีของเพื่อนร่วมงานให้รู้จักกับคนในบริษัท Kramerica
                แล้วค่อยดูท่าทีว่าเป็นอย่างไร
                แล้วจึงตัดสินใจอีกครั้งว่าจะแนะนำให้รู้จักกับอีก 3 บริษัทไหมน
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D2[5]"
                className="form-check-input"
                onClick={() => ChangeDomain2(5, 4, 0)}
              />
              <label className="form-check-label">
                d) แนะนำสามีของเพื่อนร่วมงานให้กับบริษัท Kramerica
                โดยหากได้งานขอให้เลี้ยงอาหารเป็นการตอบแทน
              </label>
            </div>
          </div>
          <div>
            <p>
              6.
              คุณเพิ่งเซ็นสัญญาเช่าพื้นที่สำนักงานใหม่และมีกำหนดการที่จะย้ายเข้าอีก
              3 เดือนข้างหน้า
              แต่คุณได้รับอีเมล์จากนายหน้าว่าผู้เช่าอยู่ตอนนี้ได้ย้ายออกก่อนกำหนด
              ขณะนี้พื้นที่สำนักงานนั้นจึงว่างแล้ว
              คุณดีใจที่จะได้ย้ายเข้าเพราะสำนักงานใหม่ที่ดีกว่า
              แต่นายหน้าคิดว่าคุณต้องการรอจนกว่าจะถึงวันกำหนดย้ายเข้า
              คุณรู้ว่านายหน้าไม่อยากให้สำนักงานว่างทิ้งเป็นเวลา 3 เดือน
              เพราะเขาก็จะไม่มีรายได้เลย คุณจะ…
            </p>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D2[6]"
                className="form-check-input"
                onClick={() => ChangeDomain2(6, 1, 0)}
              />
              <label className="form-check-label">
                a)
                คุณพร้อมที่จะย้ายเข้าไปก่อนถ้าสำนักงานใหม่ช่วยปรับราคาให้เท่าก้บที่เก่าของคุณหน่อย
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D2[6]"
                className="form-check-input"
                onClick={() => ChangeDomain2(6, 2, 0)}
              />
              <label className="form-check-label">
                b) จริงๆ แล้วคุณอยากที่จะรอจนถึงกำหนดมากกว่า
                แต่คุณสามารถย้ายเข้าไปได้ ถ้าลดค่าเช่าให้มากกว่านี้
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D2[6]"
                className="form-check-input"
                onClick={() => ChangeDomain2(6, 3, 1)}
              />
              <label className="form-check-label">
                c) คุณยินดีที่จะย้ายเข้าเลยและพร้อมที่จะช่วยแบ่งเบาค่าเช่าช่วง 3
                เดือนของผู้เช่าคนเก่านั้น
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D2[6]"
                className="form-check-input"
                onClick={() => ChangeDomain2(6, 4, 0)}
              />
              <label className="form-check-label">
                d) เสนอนายหน้าว่าจะช่วยหาคนมาอยู่ช่วงสามเดือนนั้น
                แต่ขอแบ่งเปอร์เซนต์จากค่าเช่าตรงนั้น
              </label>
            </div>
          </div>
          <div ref={myRefdomain3}>
            <h4 className="mt-5">โดเมนที่ 3</h4>
          </div>
          <div>
            <p>
              1.
              ฉันชอบที่จะพูดคุยปรึกษาถึงปัญหาและทางแก้ไขกับผู้อื่นมากกว่าที่จะคิดคนเดียว
            </p>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D3[1]"
                className="form-check-input"
                onClick={() => ChangeDomain3(1, -3)}
              />
              <label className="form-check-label">ไม่เห็นด้วยอย่างยิ่ง</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D3[1]"
                className="form-check-input"
                onClick={() => ChangeDomain3(1, -2)}
              />
              <label className="form-check-label">ไม่เห็นด้วย</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D3[1]"
                className="form-check-input"
                onClick={() => ChangeDomain3(1, -1)}
              />
              <label className="form-check-label">
                ไม่เห็นด้วยเป็นบ่างอย่าง
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D3[1]"
                className="form-check-input"
                onClick={() => ChangeDomain3(1, 0)}
              />
              <label className="form-check-label">เฉยๆ</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D3[1]"
                className="form-check-input"
                onClick={() => ChangeDomain3(1, 1)}
              />
              <label className="form-check-label">เห็นด้วยเป็นบางอย่าง</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D3[1]"
                className="form-check-input"
                onClick={() => ChangeDomain3(1, 2)}
              />
              <label className="form-check-label">เห็นด้วย</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D3[1]"
                className="form-check-input"
                onClick={() => ChangeDomain3(1, 3)}
              />
              <label className="form-check-label">เห็นด้วยอย่างยิ่ง</label>
            </div>
          </div>
          <div>
            <p>2. การทำงานร่วมกับคนอื่นทำให้ฉันทำงานช้าลง</p>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D3[2]"
                className="form-check-input"
                onClick={() => ChangeDomain3(2, 3)}
              />
              <label className="form-check-label">ไม่เห็นด้วยอย่างยิ่ง</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D3[2]"
                className="form-check-input"
                onClick={() => ChangeDomain3(2, 2)}
              />
              <label className="form-check-label">ไม่เห็นด้วย</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D3[2]"
                className="form-check-input"
                onClick={() => ChangeDomain3(2, 1)}
              />
              <label className="form-check-label">
                ไม่เห็นด้วยเป็นบ่างอย่าง
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D3[2]"
                className="form-check-input"
                onClick={() => ChangeDomain3(2, 0)}
              />
              <label className="form-check-label">เฉยๆ</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D3[2]"
                className="form-check-input"
                onClick={() => ChangeDomain3(2, -1)}
              />
              <label className="form-check-label">เห็นด้วยเป็นบางอย่าง</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D3[2]"
                className="form-check-input"
                onClick={() => ChangeDomain3(2, -2)}
              />
              <label className="form-check-label">เห็นด้วย</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D3[2]"
                className="form-check-input"
                onClick={() => ChangeDomain3(2, -3)}
              />
              <label className="form-check-label">เห็นด้วยอย่างยิ่ง</label>
            </div>
          </div>
          <div>
            <p>3. ฉันรู้สึกว่าความคิดเห็นของฉันจะไม่สำคัญเท่ากับของคนอื่น</p>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D3[3]"
                className="form-check-input"
                onClick={() => ChangeDomain3(3, 3)}
              />
              <label className="form-check-label">ไม่เห็นด้วยอย่างยิ่ง</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D3[3]"
                className="form-check-input"
                onClick={() => ChangeDomain3(3, 2)}
              />
              <label className="form-check-label">ไม่เห็นด้วย</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D3[3]"
                className="form-check-input"
                onClick={() => ChangeDomain3(3, 1)}
              />
              <label className="form-check-label">
                ไม่เห็นด้วยเป็นบ่างอย่าง
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D3[3]"
                className="form-check-input"
                onClick={() => ChangeDomain3(3, 0)}
              />
              <label className="form-check-label">เฉยๆ</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D3[3]"
                className="form-check-input"
                onClick={() => ChangeDomain3(3, -1)}
              />
              <label className="form-check-label">เห็นด้วยเป็นบางอย่าง</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D3[3]"
                className="form-check-input"
                onClick={() => ChangeDomain3(3, -2)}
              />
              <label className="form-check-label">เห็นด้วย</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D3[3]"
                className="form-check-input"
                onClick={() => ChangeDomain3(3, -3)}
              />
              <label className="form-check-label">เห็นด้วยอย่างยิ่ง</label>
            </div>
          </div>
          <div>
            <p>
              4. ฉันคิดว่างานจะประสบความสำเร็จได้
              ถ้าสมาชิกในทีมเลือกทำงานที่ตัวเองถนัดที่สุดมากกว่าสิ่งที่คนอื่นอยากให้ทำ
            </p>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D3[4]"
                className="form-check-input"
                onClick={() => ChangeDomain3(4, 3)}
              />
              <label className="form-check-label">ไม่เห็นด้วยอย่างยิ่ง</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D3[4]"
                className="form-check-input"
                onClick={() => ChangeDomain3(4, 2)}
              />
              <label className="form-check-label">ไม่เห็นด้วย</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D3[4]"
                className="form-check-input"
                onClick={() => ChangeDomain3(4, 1)}
              />
              <label className="form-check-label">
                ไม่เห็นด้วยเป็นบ่างอย่าง
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D3[4]"
                className="form-check-input"
                onClick={() => ChangeDomain3(4, 0)}
              />
              <label className="form-check-label">เฉยๆ</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D3[4]"
                className="form-check-input"
                onClick={() => ChangeDomain3(4, -1)}
              />
              <label className="form-check-label">เห็นด้วยเป็นบางอย่าง</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D3[4]"
                className="form-check-input"
                onClick={() => ChangeDomain3(4, -2)}
              />
              <label className="form-check-label">เห็นด้วย</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D3[4]"
                className="form-check-input"
                onClick={() => ChangeDomain3(4, -3)}
              />
              <label className="form-check-label">เห็นด้วยอย่างยิ่ง</label>
            </div>
          </div>
          <div>
            <p>5. ฉันไม่รู้สึกขุ่นเคืองใจถ้ามีคนติในข้อผิดพลาดของฉัน</p>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D3[5]"
                className="form-check-input"
                onClick={() => ChangeDomain3(5, -3)}
              />
              <label className="form-check-label">ไม่เห็นด้วยอย่างยิ่ง</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D3[5]"
                className="form-check-input"
                onClick={() => ChangeDomain3(5, -2)}
              />
              <label className="form-check-label">ไม่เห็นด้วย</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D3[5]"
                className="form-check-input"
                onClick={() => ChangeDomain3(5, -1)}
              />
              <label className="form-check-label">
                ไม่เห็นด้วยเป็นบ่างอย่าง
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D3[5]"
                className="form-check-input"
                onClick={() => ChangeDomain3(5, 0)}
              />
              <label className="form-check-label">เฉยๆ</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D3[5]"
                className="form-check-input"
                onClick={() => ChangeDomain3(5, 1)}
              />
              <label className="form-check-label">เห็นด้วยเป็นบางอย่าง</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D3[5]"
                className="form-check-input"
                onClick={() => ChangeDomain3(5, 2)}
              />
              <label className="form-check-label">เห็นด้วย</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D3[5]"
                className="form-check-input"
                onClick={() => ChangeDomain3(5, 3)}
              />
              <label className="form-check-label">เห็นด้วยอย่างยิ่ง</label>
            </div>
          </div>
          <div>
            <p>6. ฉันมักต้องการความเห็นชอบและการสนับสนุนจากผู้อื่นในการทำงาน</p>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D3[6]"
                className="form-check-input"
                onClick={() => ChangeDomain3(6, -3)}
              />
              <label className="form-check-label">ไม่เห็นด้วยอย่างยิ่ง</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D3[6]"
                className="form-check-input"
                onClick={() => ChangeDomain3(6, -2)}
              />
              <label className="form-check-label">ไม่เห็นด้วย</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D3[6]"
                className="form-check-input"
                onClick={() => ChangeDomain3(6, -1)}
              />
              <label className="form-check-label">
                ไม่เห็นด้วยเป็นบ่างอย่าง
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D3[6]"
                className="form-check-input"
                onClick={() => ChangeDomain3(6, 0)}
              />
              <label className="form-check-label">เฉยๆ</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D3[6]"
                className="form-check-input"
                onClick={() => ChangeDomain3(6, 1)}
              />
              <label className="form-check-label">เห็นด้วยเป็นบางอย่าง</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D3[6]"
                className="form-check-input"
                onClick={() => ChangeDomain3(6, 2)}
              />
              <label className="form-check-label">เห็นด้วย</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D3[6]"
                className="form-check-input"
                onClick={() => ChangeDomain3(6, 3)}
              />
              <label className="form-check-label">เห็นด้วยอย่างยิ่ง</label>
            </div>
          </div>
          <div ref={myRefdomain4}>
            <h4 className="mt-5">โดเมนที่ 4</h4>
          </div>
          <div>
            <p>1. ฉันสามารถบอกได้เมื่อสื่อสารไปแล้วอีกฝ่ายไม่เข้าใจ</p>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D4[1]"
                className="form-check-input"
                onClick={() => ChangeDomain4(1, -3)}
              />
              <label className="form-check-label">ไม่เห็นด้วยอย่างยิ่ง</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D4[1]"
                className="form-check-input"
                onClick={() => ChangeDomain4(1, -2)}
              />
              <label className="form-check-label">ไม่เห็นด้วย</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D4[1]"
                className="form-check-input"
                onClick={() => ChangeDomain4(1, -1)}
              />
              <label className="form-check-label">
                ไม่เห็นด้วยเป็นบ่างอย่าง
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D4[1]"
                className="form-check-input"
                onClick={() => ChangeDomain4(1, 0)}
              />
              <label className="form-check-label">เฉยๆ</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D4[1]"
                className="form-check-input"
                onClick={() => ChangeDomain4(1, 1)}
              />
              <label className="form-check-label">เห็นด้วยเป็นบางอย่าง</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D4[1]"
                className="form-check-input"
                onClick={() => ChangeDomain4(1, 2)}
              />
              <label className="form-check-label">เห็นด้วย</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D4[1]"
                className="form-check-input"
                onClick={() => ChangeDomain4(1, 3)}
              />
              <label className="form-check-label">เห็นด้วยอย่างยิ่ง</label>
            </div>
          </div>
          <div>
            <p>2. คนเข้าใจสารที่ฉันสื่อออกไปคลาดเคลื่อน</p>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D4[2]"
                className="form-check-input"
                onClick={() => ChangeDomain4(2, 3)}
              />
              <label className="form-check-label">ไม่เห็นด้วยอย่างยิ่ง</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D4[2]"
                className="form-check-input"
                onClick={() => ChangeDomain4(2, 2)}
              />
              <label className="form-check-label">ไม่เห็นด้วย</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D4[2]"
                className="form-check-input"
                onClick={() => ChangeDomain4(2, 1)}
              />
              <label className="form-check-label">
                ไม่เห็นด้วยเป็นบ่างอย่าง
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D4[2]"
                className="form-check-input"
                onClick={() => ChangeDomain4(2, 0)}
              />
              <label className="form-check-label">เฉยๆ</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D4[2]"
                className="form-check-input"
                onClick={() => ChangeDomain4(2, -1)}
              />
              <label className="form-check-label">เห็นด้วยเป็นบางอย่าง</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D4[2]"
                className="form-check-input"
                onClick={() => ChangeDomain4(2, -2)}
              />
              <label className="form-check-label">เห็นด้วย</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D4[2]"
                className="form-check-input"
                onClick={() => ChangeDomain4(2, -3)}
              />
              <label className="form-check-label">เห็นด้วยอย่างยิ่ง</label>
            </div>
          </div>
          <div>
            <p>3. ฉันคิดว่าค่อนข้างยากที่จะอธิบายความคิดในรูปแบบคำพูด</p>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D4[3]"
                className="form-check-input"
                onClick={() => ChangeDomain4(3, 3)}
              />
              <label className="form-check-label">ไม่เห็นด้วยอย่างยิ่ง</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D4[3]"
                className="form-check-input"
                onClick={() => ChangeDomain4(3, 2)}
              />
              <label className="form-check-label">ไม่เห็นด้วย</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D4[3]"
                className="form-check-input"
                onClick={() => ChangeDomain4(3, 1)}
              />
              <label className="form-check-label">
                ไม่เห็นด้วยเป็นบ่างอย่าง
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D4[3]"
                className="form-check-input"
                onClick={() => ChangeDomain4(3, 0)}
              />
              <label className="form-check-label">เฉยๆ</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D4[3]"
                className="form-check-input"
                onClick={() => ChangeDomain4(3, -1)}
              />
              <label className="form-check-label">เห็นด้วยเป็นบางอย่าง</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D4[3]"
                className="form-check-input"
                onClick={() => ChangeDomain4(3, -2)}
              />
              <label className="form-check-label">เห็นด้วย</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D4[3]"
                className="form-check-input"
                onClick={() => ChangeDomain4(3, -3)}
              />
              <label className="form-check-label">เห็นด้วยอย่างยิ่ง</label>
            </div>
          </div>
          <div>
            <p>
              4.
              ฉันต้องพูดย้ำหลายครั้งเพราะคนอื่นไม่เข้าใจสิ่งที่ฉันพูดตั้งแต่ครั้งแรก
            </p>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D4[4]"
                className="form-check-input"
                onClick={() => ChangeDomain4(4, 3)}
              />
              <label className="form-check-label">ไม่เห็นด้วยอย่างยิ่ง</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D4[4]"
                className="form-check-input"
                onClick={() => ChangeDomain4(4, 2)}
              />
              <label className="form-check-label">ไม่เห็นด้วย</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D4[4]"
                className="form-check-input"
                onClick={() => ChangeDomain4(4, 1)}
              />
              <label className="form-check-label">
                ไม่เห็นด้วยเป็นบ่างอย่าง
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D4[4]"
                className="form-check-input"
                onClick={() => ChangeDomain4(4, 0)}
              />
              <label className="form-check-label">เฉยๆ</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D4[4]"
                className="form-check-input"
                onClick={() => ChangeDomain4(4, -1)}
              />
              <label className="form-check-label">เห็นด้วยเป็นบางอย่าง</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D4[4]"
                className="form-check-input"
                onClick={() => ChangeDomain4(4, -2)}
              />
              <label className="form-check-label">เห็นด้วย</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D4[4]"
                className="form-check-input"
                onClick={() => ChangeDomain4(4, -3)}
              />
              <label className="form-check-label">เห็นด้วยอย่างยิ่ง</label>
            </div>
          </div>
          <div>
            <p>5. เวลาสนทนากันฉันสามารถรับรู้ถึงอารมณ์ของอีกฝ่ายได้</p>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D4[5]"
                className="form-check-input"
                onClick={() => ChangeDomain4(5, -3)}
              />
              <label className="form-check-label">ไม่เห็นด้วยอย่างยิ่ง</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D4[5]"
                className="form-check-input"
                onClick={() => ChangeDomain4(5, -2)}
              />
              <label className="form-check-label">ไม่เห็นด้วย</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D4[5]"
                className="form-check-input"
                onClick={() => ChangeDomain4(5, -1)}
              />
              <label className="form-check-label">
                ไม่เห็นด้วยเป็นบ่างอย่าง
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D4[5]"
                className="form-check-input"
                onClick={() => ChangeDomain4(5, 0)}
              />
              <label className="form-check-label">เฉยๆ</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D4[5]"
                className="form-check-input"
                onClick={() => ChangeDomain4(5, 1)}
              />
              <label className="form-check-label">เห็นด้วยเป็นบางอย่าง</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D4[5]"
                className="form-check-input"
                onClick={() => ChangeDomain4(5, 2)}
              />
              <label className="form-check-label">เห็นด้วย</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D4[5]"
                className="form-check-input"
                onClick={() => ChangeDomain4(5, 3)}
              />
              <label className="form-check-label">เห็นด้วยอย่างยิ่ง</label>
            </div>
          </div>
          <div>
            <p>
              6. เมื่อฉันไม่เข้าใจถึงคำอธิบาย ฉันเขินอาย/
              ไม่สะดวกใจที่จะถามให้ชัดเจน
            </p>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D4[6]"
                className="form-check-input"
                onClick={() => ChangeDomain4(6, 3)}
              />
              <label className="form-check-label">ไม่เห็นด้วยอย่างยิ่ง</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D4[6]"
                className="form-check-input"
                onClick={() => ChangeDomain4(6, 2)}
              />
              <label className="form-check-label">ไม่เห็นด้วย</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D4[6]"
                className="form-check-input"
                onClick={() => ChangeDomain4(6, 1)}
              />
              <label className="form-check-label">
                ไม่เห็นด้วยเป็นบ่างอย่าง
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D4[6]"
                className="form-check-input"
                onClick={() => ChangeDomain4(6, 0)}
              />
              <label className="form-check-label">เฉยๆ</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                className="form-check-input"
                onClick={() => ChangeDomain4(6, -1)}
              />
              <label className="form-check-label">เห็นด้วยเป็นบางอย่าง</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D4[6]"
                className="form-check-input"
                onClick={() => ChangeDomain4(6, -2)}
              />
              <label className="form-check-label">เห็นด้วย</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D4[6]"
                className="form-check-input"
                onClick={() => ChangeDomain4(6, -3)}
              />
              <label className="form-check-label">เห็นด้วยอย่างยิ่ง</label>
            </div>
          </div>
          <div ref={myRefdomain5}>
            <h4 className="mt-5">โดเมนที่ 5</h4>
          </div>
          <div>
            <p>1. เมื่อได้รับมอบหมายงานที่คุณไม่ถนัด คุณจะ</p>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D5[1]"
                className="form-check-input"
                onClick={() => ChangeDomain5(1, 1, 1)}
              />
              <label className="form-check-label">
                a) พยายามศึกษาค้นหาข้อมูลเพื่อนำมาปรับใช้
                และคอยถามเพิ่มเติมจากคนที่ถนัดด้านนี้
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D5[1]"
                className="form-check-input"
                onClick={() => ChangeDomain5(1, 2, 0)}
              />
              <label className="form-check-label">
                b) ขอความช่วยเหลือจากคนที่คาดว่าเขาน่าจะถนัดกับงานนี้
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D5[1]"
                className="form-check-input"
                onClick={() => ChangeDomain5(1, 3, 0)}
              />
              <label className="form-check-label">
                c) เสนอตัวขอทำงานอื่นที่คิดว่าน่าจะเหมาะกับความสามารถมากกว่า
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D5[1]"
                className="form-check-input"
                onClick={() => ChangeDomain5(1, 4, 0)}
              />
              <label className="form-check-label">
                d)
                แจ้งให้หัวหน้างานทราบว่างานที่คุณได้รับมอบหมายไม่อยู่ในขอบเขตที่จะทำได้
              </label>
            </div>
          </div>
          <div>
            <p>
              2. เมื่อคุณรับออเดอร์สินค้า limited edition จากลูกค้าเรียบร้อยแล้ว
              กลับพบว่าสินค้าชิ้นสุดท้ายเสียหายไม่สามารถขายให้กับลูกค้าได้
              และเป็นสินค้าที่ไม่ผลิตออกมาอีกแล้ว คุณจะ
            </p>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D5[2]"
                className="form-check-input"
                onClick={() => ChangeDomain5(2, 1, 0)}
              />
              <label className="form-check-label">
                a) พยายามหาสินค้าอื่นที่ใกล้เคียงกัน
                และนำเสนอสินค้านั้นให้แก่ลูกค้าแทน
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D5[2]"
                className="form-check-input"
                onClick={() => ChangeDomain5(2, 2, 0)}
              />
              <label className="form-check-label">
                b) โน้มน้าวให้ลูกค้าเลือกซื้อสินค้าชิ้นอื่นพร้อมเสนอส่วนลดให้
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D5[2]"
                className="form-check-input"
                onClick={() => ChangeDomain5(2, 3, 1)}
              />
              <label className="form-check-label">
                c)
                แจ้งลูกค้าอย่างตรงไปตรงมาว่าสินค้าเสียหายไม่สามารถขายให้ลูกค้าได้แม้ว่าจะเสี่ยงที่จะโดนลูกค้าตำหนิ
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D5[2]"
                className="form-check-input"
                onClick={() => ChangeDomain5(2, 4, 0)}
              />
              <label className="form-check-label">
                d)
                พยายามซ่อมแซมดัดแปลงส่วนที่เสียหายอย่างประณีตเพื่อให้ลูกค้าได้สินค้าที่สมบูรณ์ที่สุด
              </label>
            </div>
          </div>
          <div>
            <p>
              3. ขณะที่ต้องนำเสนอโปรเจ็คสำคัญของคุณในอีกสามวันข้างหน้า
              ยังเหลืองานที่ต้องทำอีกพอสมควร
              แต่เพื่อนร่วมทีมลงความเห็นว่าไม่สามารถทำเสร็จได้ คุณจะ...
            </p>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D5[3]"
                className="form-check-input"
                onClick={() => ChangeDomain5(3, 1, 1)}
              />
              <label className="form-check-label">
                a) โหมงานหนักตลอดชั่วโมงการทำงาน
                และผลักดันเพื่อนรวมงานให้ทำงานอย่างเต็มที่
                เพื่อให้งานเสร็จทันเวลา
                เพราะคิดว่าถ้าทุ่มสุดตัวงานต้องเสร็จทันเวลาแน่ๆ
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D5[3]"
                className="form-check-input"
                onClick={() => ChangeDomain5(3, 2, 0)}
              />
              <label className="form-check-label">
                b)
                ปรึกษาหัวหน้างานระดับสูงถึงเหตุการณ์ที่เกิดขึ้นและขอความคิดเห็นถึงแนวทางที่ควรจะทำต่อไป
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D5[3]"
                className="form-check-input"
                onClick={() => ChangeDomain5(3, 3, 0)}
              />
              <label className="form-check-label">
                c) ขอความช่วยเหลือจากเพื่อนร่วมงานทีมอื่นให้มาทำงานชิ้นนี้
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D5[3]"
                className="form-check-input"
                onClick={() => ChangeDomain5(3, 4, 0)}
              />
              <label className="form-check-label">
                d)
                เห็นด้วยกับเพื่อนร่วมทีมว่างานคงเสร็จไม่ทันจึงขอเลื่อนเวลาส่งงานเพื่อป้องกันเหตุผิดพลาดที่จะส่งงานที่ไม่สมบูรณ์
              </label>
            </div>
          </div>
          <div>
            <p>
              4. เมื่อคุณทำงานตามที่ลูกค้าบรีฟมาเรียบร้อย
              แต่เมื่อวันส่งงานลูกค้าเปลี่ยนใจต้องการงานอีกแบบที่ต่างกันอย่างสิ้นเชิง
              คุณจะ
            </p>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D5[4]"
                className="form-check-input"
                onClick={() => ChangeDomain5(4, 1, 1)}
              />
              <label className="form-check-label">
                a) ปรับงานตามที่ลูกค้าบรีฟใหม่ให้ตรงกับความต้องการลูกค้า
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D5[4]"
                className="form-check-input"
                onClick={() => ChangeDomain5(4, 2, 0)}
              />
              <label className="form-check-label">
                b)
                ชี้แจ้งให้ลูกค้าทราบว่าได้ทำตามงานที่ลูกค้าบรีฟครั้งที่แล้วมาเสร็จเรียบร้อยแล้ว
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D5[4]"
                className="form-check-input"
                onClick={() => ChangeDomain5(4, 3, 0)}
              />
              <label className="form-check-label">
                c)
                อธิบายให้ลูกค้าเข้าใจว่างานจะต้องทำตามที่บรีฟไว้ตอนแรกเท่านั้นเนื่องจากได้ตกลงเอาไว้แล้ว
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D5[4]"
                className="form-check-input"
                onClick={() => ChangeDomain5(4, 4, 0)}
              />
              <label className="form-check-label">
                d)
                ยินดีแก้งานให้ลูกค้าตามที่บรีฟใหม่แต่แจ้งลูกค้าให้ทราบว่าลูกค้าต้องเสียค่าใช้จ่ายเพิ่ม
              </label>
            </div>
          </div>
          <div>
            <p>
              5. เมื่อวันที่มีนำเสนองานแก่ลูกค้า
              เนื่องจากรถติดมากคุณกำลังจะไปสาย
              ก่อนไปหาลูกค้าคุณเหลือบไปเห็นร้านน้ำมะพร้าวชื่อดังที่มีคนต่อแถวยาวเหยียดที่ลูกค้าชอบกิน
              คุณจะ
            </p>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D5[5]"
                className="form-check-input"
                onClick={() => ChangeDomain5(5, 1, 0)}
              />
              <label className="form-check-label">
                a) จอดซื้อน้ำมะพร้าวเพื่อเอาใจลูกค้า
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D5[5]"
                className="form-check-input"
                onClick={() => ChangeDomain5(5, 2, 0)}
              />
              <label className="form-check-label">
                b) โทรแจ้งลูกค้าว่ารถติดทำให้ต้องไปสาย
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D5[5]"
                className="form-check-input"
                onClick={() => ChangeDomain5(5, 3, 1)}
              />
              <label className="form-check-label">
                c) พยายามหาทางลัดเพื่อให้สามารถไปถึงที่หมายให้เร็วที่สุด
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D5[5]"
                className="form-check-input"
                onClick={() => ChangeDomain5(5, 4, 0)}
              />
              <label className="form-check-label">
                d)
                โทรไปเลื่อนนัดลูกค้าโดยแจ้งว่าติดธุระสำคัญเพื่อไม่ให้ลูกค้าเสียความรู้สึกว่าคุณไปสาย
              </label>
            </div>
          </div>
          <div>
            <p>
              6.
              ถ้าหากคุณเป็นหมอแล้วมีผู้ป่วยสองคนเข้ารับการรักษาในเวลาไล่เลี่ยกันโดยคนแรกเป็นนักเลงที่ถูกฟันเข้าที่กลางหลังกำลังเสียเลือดมาก
              และอีกคนเป็นแม่ค้าเขียงหมูที่ตลาดถูกมีดสับเข้าที่นิ้วชี้ คุณจะ…
            </p>
            <div className="form-check  ml-5">
              <input
                type="radio"
                className="form-check-input"
                name="D5[6]"
                onClick={() => ChangeDomain5(6, 1, 1)}
              />
              <label className="form-check-label">
                a) รักษานักเลงก่อนเพราะกำลังเสียเลือดมาก
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                className="form-check-input"
                name="D5[6]"
                onClick={() => ChangeDomain5(6, 2, 0)}
              />
              <label className="form-check-label">
                b)
                รักษานักเลงก่อนป้องกันเพื่อนของนักเลงอาละวาดรบกวนผู้ป่วยคนอื่น
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                className="form-check-input"
                name="D5[6]"
                onClick={() => ChangeDomain5(6, 3, 0)}
              />
              <label className="form-check-label">
                c) รักษาแม่ค้าก่อนเพราะสามารถเย็บแผลได้เร็วกว่า
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D5[6]"
                className="form-check-input"
                onClick={() => ChangeDomain5(6, 4, 0)}
              />
              <label className="form-check-label">
                d)
                รักษาแม่ค้าก่อนเพราะแม่ค้าเป็นคนจิตใจดีชอบเลี้ยงอาหารเด็กด้อยโอกาส
              </label>
            </div>
          </div>
          <div ref={myRefdomain6}>
            <h4 className="mt-5">โดเมนที่ 6</h4>
          </div>
          <div>
            <p>1. ฉันสามารถบอกได้เมื่อสื่อสารไปแล้วอีกฝ่ายไม่เข้าใจ</p>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D6[1]"
                className="form-check-input"
                onClick={() => ChangeDomain6(1, -3)}
              />
              <label className="form-check-label">ไม่เห็นด้วยอย่างยิ่ง</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D6[1]"
                className="form-check-input"
                onClick={() => ChangeDomain6(1, -2)}
              />
              <label className="form-check-label">ไม่เห็นด้วย</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D6[1]"
                className="form-check-input"
                onClick={() => ChangeDomain6(1, -1)}
              />
              <label className="form-check-label">
                ไม่เห็นด้วยเป็นบ่างอย่าง
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D6[1]"
                className="form-check-input"
                onClick={() => ChangeDomain6(1, 0)}
              />
              <label className="form-check-label">เฉยๆ</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D6[1]"
                className="form-check-input"
                onClick={() => ChangeDomain6(1, 1)}
              />
              <label className="form-check-label">เห็นด้วยเป็นบางอย่าง</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D6[1]"
                className="form-check-input"
                onClick={() => ChangeDomain6(1, 2)}
              />
              <label className="form-check-label">เห็นด้วย</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D6[1]"
                className="form-check-input"
                onClick={() => ChangeDomain6(1, 3)}
              />
              <label className="form-check-label">เห็นด้วยอย่างยิ่ง</label>
            </div>
          </div>
          <div>
            <p>2. คนเข้าใจสารที่ฉันสื่อออกไปคลาดเคลื่อน</p>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D6[2]"
                className="form-check-input"
                onClick={() => ChangeDomain6(2, -3)}
              />
              <label className="form-check-label">ไม่เห็นด้วยอย่างยิ่ง</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D6[2]"
                className="form-check-input"
                onClick={() => ChangeDomain6(2, -2)}
              />
              <label className="form-check-label">ไม่เห็นด้วย</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D6[2]"
                className="form-check-input"
                onClick={() => ChangeDomain6(2, -1)}
              />
              <label className="form-check-label">
                ไม่เห็นด้วยเป็นบ่างอย่าง
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D6[2]"
                className="form-check-input"
                onClick={() => ChangeDomain6(2, 0)}
              />
              <label className="form-check-label">เฉยๆ</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D6[2]"
                className="form-check-input"
                onClick={() => ChangeDomain6(2, 1)}
              />
              <label className="form-check-label">เห็นด้วยเป็นบางอย่าง</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D6[2]"
                className="form-check-input"
                onClick={() => ChangeDomain6(2, 2)}
              />
              <label className="form-check-label">เห็นด้วย</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D6[2]"
                className="form-check-input"
                onClick={() => ChangeDomain6(2, 3)}
              />
              <label className="form-check-label">เห็นด้วยอย่างยิ่ง</label>
            </div>
          </div>
          <div>
            <p>3. ฉันคิดว่าค่อนข้างยากที่จะอธิบายความคิดในรูปแบบคำพูด</p>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D6[3]"
                className="form-check-input"
                onClick={() => ChangeDomain6(3, 3)}
              />
              <label className="form-check-label">ไม่เห็นด้วยอย่างยิ่ง</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D6[3]"
                className="form-check-input"
                onClick={() => ChangeDomain6(3, 2)}
              />
              <label className="form-check-label">ไม่เห็นด้วย</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D6[3]"
                className="form-check-input"
                onClick={() => ChangeDomain6(3, 1)}
              />
              <label className="form-check-label">
                ไม่เห็นด้วยเป็นบ่างอย่าง
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D6[3]"
                className="form-check-input"
                onClick={() => ChangeDomain6(3, 0)}
              />
              <label className="form-check-label">เฉยๆ</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D6[3]"
                className="form-check-input"
                onClick={() => ChangeDomain6(3, -1)}
              />
              <label className="form-check-label">เห็นด้วยเป็นบางอย่าง</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D6[3]"
                className="form-check-input"
                onClick={() => ChangeDomain6(3, -2)}
              />
              <label className="form-check-label">เห็นด้วย</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D6[3]"
                className="form-check-input"
                onClick={() => ChangeDomain6(3, -3)}
              />
              <label className="form-check-label">เห็นด้วยอย่างยิ่ง</label>
            </div>
          </div>
          <div>
            <p>
              4.
              ฉันต้องพูดย้ำหลายครั้งเพราะคนอื่นไม่เข้าใจสิ่งที่ฉันพูดตั้งแต่ครั้งแรก
            </p>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D6[4]"
                className="form-check-input"
                onClick={() => ChangeDomain6(4, -3)}
              />
              <label className="form-check-label">ไม่เห็นด้วยอย่างยิ่ง</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D6[4]"
                className="form-check-input"
                onClick={() => ChangeDomain6(4, -2)}
              />
              <label className="form-check-label">ไม่เห็นด้วย</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D6[4]"
                className="form-check-input"
                onClick={() => ChangeDomain6(4, -1)}
              />
              <label className="form-check-label">
                ไม่เห็นด้วยเป็นบ่างอย่าง
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D6[4]"
                className="form-check-input"
                onClick={() => ChangeDomain6(4, 0)}
              />
              <label className="form-check-label">เฉยๆ</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D6[4]"
                className="form-check-input"
                onClick={() => ChangeDomain6(4, 1)}
              />
              <label className="form-check-label">เห็นด้วยเป็นบางอย่าง</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D6[4]"
                className="form-check-input"
                onClick={() => ChangeDomain6(4, 2)}
              />
              <label className="form-check-label">เห็นด้วย</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D6[4]"
                className="form-check-input"
                onClick={() => ChangeDomain6(4, 3)}
              />
              <label className="form-check-label">เห็นด้วยอย่างยิ่ง</label>
            </div>
          </div>
          <div>
            <p>5. เวลาสนทนากันฉันสามารถรับรู้ถึงอารมณ์ของอีกฝ่ายได้</p>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D6[5]"
                className="form-check-input"
                onClick={() => ChangeDomain6(5, -3)}
              />
              <label className="form-check-label">ไม่เห็นด้วยอย่างยิ่ง</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D6[5]"
                className="form-check-input"
                onClick={() => ChangeDomain6(5, -2)}
              />
              <label className="form-check-label">ไม่เห็นด้วย</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D6[5]"
                className="form-check-input"
                onClick={() => ChangeDomain6(5, -1)}
              />
              <label className="form-check-label">
                ไม่เห็นด้วยเป็นบ่างอย่าง
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D6[5]"
                className="form-check-input"
                onClick={() => ChangeDomain6(5, 0)}
              />
              <label className="form-check-label">เฉยๆ</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D6[5]"
                className="form-check-input"
                onClick={() => ChangeDomain6(5, 1)}
              />
              <label className="form-check-label">เห็นด้วยเป็นบางอย่าง</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D6[5]"
                className="form-check-input"
                onClick={() => ChangeDomain6(5, 2)}
              />
              <label className="form-check-label">เห็นด้วย</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D6[5]"
                className="form-check-input"
                onClick={() => ChangeDomain6(5, 3)}
              />
              <label className="form-check-label">เห็นด้วยอย่างยิ่ง</label>
            </div>
          </div>
          <div>
            <p>
              6. เมื่อฉันไม่เข้าใจถึงคำอธิบาย ฉันเขินอาย/
              ไม่สะดวกใจที่จะถามให้ชัดเจน
            </p>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D6[6]"
                className="form-check-input"
                onClick={() => ChangeDomain6(6, 3)}
              />
              <label className="form-check-label">ไม่เห็นด้วยอย่างยิ่ง</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D6[6]"
                className="form-check-input"
                onClick={() => ChangeDomain6(6, 2)}
              />
              <label className="form-check-label">ไม่เห็นด้วย</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D6[6]"
                className="form-check-input"
                onClick={() => ChangeDomain6(6, 1)}
              />
              <label className="form-check-label">
                ไม่เห็นด้วยเป็นบ่างอย่าง
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D6[6]"
                className="form-check-input"
                onClick={() => ChangeDomain6(6, 0)}
              />
              <label className="form-check-label">เฉยๆ</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D6[6]"
                className="form-check-input"
                onClick={() => ChangeDomain6(6, -1)}
              />
              <label className="form-check-label">เห็นด้วยเป็นบางอย่าง</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D6[6]"
                className="form-check-input"
                onClick={() => ChangeDomain6(6, -2)}
              />
              <label className="form-check-label">เห็นด้วย</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D6[6]"
                className="form-check-input"
                onClick={() => ChangeDomain6(6, -3)}
              />
              <label className="form-check-label">เห็นด้วยอย่างยิ่ง</label>
            </div>
          </div>
          <div ref={myRefdomain7}>
            <h4 className="mt-5">โดเมนที่ 7</h4>
          </div>
          <div>
            <p>1. ความคิดหรือโครงการใหม่ๆ บางครั้งก็ทำให้ฉันเสียสมาธิ</p>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D7[1]"
                className="form-check-input"
                onClick={() => ChangeDomain7(1, 3)}
              />
              <label className="form-check-label">ไม่เห็นด้วยอย่างยิ่ง</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D7[1]"
                className="form-check-input"
                onClick={() => ChangeDomain7(1, 2)}
              />
              <label className="form-check-label">ไม่เห็นด้วย</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D7[1]"
                className="form-check-input"
                onClick={() => ChangeDomain7(1, 1)}
              />
              <label className="form-check-label">
                ไม่เห็นด้วยเป็นบ่างอย่าง
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D7[1]"
                className="form-check-input"
                onClick={() => ChangeDomain7(1, 0)}
              />
              <label className="form-check-label">เฉยๆ</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D7[1]"
                className="form-check-input"
                onClick={() => ChangeDomain7(1, -1)}
              />
              <label className="form-check-label">เห็นด้วยเป็นบางอย่าง</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D7[1]"
                className="form-check-input"
                onClick={() => ChangeDomain7(1, -2)}
              />
              <label className="form-check-label">เห็นด้วย</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D7[1]"
                className="form-check-input"
                onClick={() => ChangeDomain7(1, -3)}
              />
              <label className="form-check-label">เห็นด้วยอย่างยิ่ง</label>
            </div>
          </div>
          <div>
            <p>2. ความสนใจของฉันเปลี่ยนปีต่อปี</p>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D7[2]"
                className="form-check-input"
                onClick={() => ChangeDomain7(2, 3)}
              />
              <label className="form-check-label">ไม่เห็นด้วยอย่างยิ่ง</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D7[2]"
                className="form-check-input"
                onClick={() => ChangeDomain7(2, 2)}
              />
              <label className="form-check-label">ไม่เห็นด้วย</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D7[2]"
                className="form-check-input"
                onClick={() => ChangeDomain7(2, 1)}
              />
              <label className="form-check-label">
                ไม่เห็นด้วยเป็นบ่างอย่าง
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D7[2]"
                className="form-check-input"
                onClick={() => ChangeDomain7(2, 0)}
              />
              <label className="form-check-label">เฉยๆ</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D7[2]"
                className="form-check-input"
                onClick={() => ChangeDomain7(2, -1)}
              />
              <label className="form-check-label">เห็นด้วยเป็นบางอย่าง</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D7[2]"
                className="form-check-input"
                onClick={() => ChangeDomain7(2, -2)}
              />
              <label className="form-check-label">เห็นด้วย</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D7[2]"
                className="form-check-input"
                onClick={() => ChangeDomain7(2, -3)}
              />
              <label className="form-check-label">เห็นด้วยอย่างยิ่ง</label>
            </div>
          </div>
          <div>
            <p>3. ฉันเป็นคนที่ทำงานหนัก</p>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D7[3]"
                className="form-check-input"
                onClick={() => ChangeDomain7(3, -3)}
              />
              <label className="form-check-label">ไม่เห็นด้วยอย่างยิ่ง</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D7[3]"
                className="form-check-input"
                onClick={() => ChangeDomain7(3, -2)}
              />
              <label className="form-check-label">ไม่เห็นด้วย</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D7[3]"
                className="form-check-input"
                onClick={() => ChangeDomain7(3, -1)}
              />
              <label className="form-check-label">
                ไม่เห็นด้วยเป็นบ่างอย่าง
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D7[3]"
                className="form-check-input"
                onClick={() => ChangeDomain7(3, 0)}
              />
              <label className="form-check-label">เฉยๆ</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D7[3]"
                className="form-check-input"
                onClick={() => ChangeDomain7(3, 1)}
              />
              <label className="form-check-label">เห็นด้วยเป็นบางอย่าง</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D7[3]"
                className="form-check-input"
                onClick={() => ChangeDomain7(3, 2)}
              />
              <label className="form-check-label">เห็นด้วย</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D7[3]"
                className="form-check-input"
                onClick={() => ChangeDomain7(3, 3)}
              />
              <label className="form-check-label">เห็นด้วยอย่างยิ่ง</label>
            </div>
          </div>
          <div>
            <p>4. ฉันทำงานจนเสร็จไม่ว่าจะเป็นการเริ่มทำงานอะไรก็ตาม</p>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D7[4]"
                className="form-check-input"
                onClick={() => ChangeDomain7(4, -3)}
              />
              <label className="form-check-label">ไม่เห็นด้วยอย่างยิ่ง</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D7[4]"
                className="form-check-input"
                onClick={() => ChangeDomain7(4, -2)}
              />
              <label className="form-check-label">ไม่เห็นด้วย</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D7[4]"
                className="form-check-input"
                onClick={() => ChangeDomain7(4, -1)}
              />
              <label className="form-check-label">
                ไม่เห็นด้วยเป็นบ่างอย่าง
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D7[4]"
                className="form-check-input"
                onClick={() => ChangeDomain7(4, 0)}
              />
              <label className="form-check-label">เฉยๆ</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D7[4]"
                className="form-check-input"
                onClick={() => ChangeDomain7(4, 1)}
              />
              <label className="form-check-label">เห็นด้วยเป็นบางอย่าง</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D7[4]"
                className="form-check-input"
                onClick={() => ChangeDomain7(4, 2)}
              />
              <label className="form-check-label">เห็นด้วย</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D7[4]"
                className="form-check-input"
                onClick={() => ChangeDomain7(4, 3)}
              />
              <label className="form-check-label">เห็นด้วยอย่างยิ่ง</label>
            </div>
          </div>
          <div>
            <p>5. ฉันมองหาแรงจูงใจใหม่ๆ ในทุกๆ 2-3 เดือน</p>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D7[5]"
                className="form-check-input"
                onClick={() => ChangeDomain7(5, 3)}
              />
              <label className="form-check-label">ไม่เห็นด้วยอย่างยิ่ง</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D7[5]"
                className="form-check-input"
                onClick={() => ChangeDomain7(5, 2)}
              />
              <label className="form-check-label">ไม่เห็นด้วย</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D7[5]"
                className="form-check-input"
                onClick={() => ChangeDomain7(5, 1)}
              />
              <label className="form-check-label">
                ไม่เห็นด้วยเป็นบ่างอย่าง
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D7[5]"
                className="form-check-input"
                onClick={() => ChangeDomain7(5, 0)}
              />
              <label className="form-check-label">เฉยๆ</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D7[5]"
                className="form-check-input"
                onClick={() => ChangeDomain7(5, -1)}
              />
              <label className="form-check-label">เห็นด้วยเป็นบางอย่าง</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D7[5]"
                className="form-check-input"
                onClick={() => ChangeDomain7(5, -2)}
              />
              <label className="form-check-label">เห็นด้วย</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D7[5]"
                className="form-check-input"
                onClick={() => ChangeDomain7(5, -3)}
              />
              <label className="form-check-label">เห็นด้วยอย่างยิ่ง</label>
            </div>
          </div>
          <div>
            <p>6. ฉันเป็นคนขยัน</p>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D7[6]"
                className="form-check-input"
                onClick={() => ChangeDomain7(6, -3)}
              />
              <label className="form-check-label">ไม่เห็นด้วยอย่างยิ่ง</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D7[6]"
                className="form-check-input"
                onClick={() => ChangeDomain7(6, -2)}
              />
              <label className="form-check-label">ไม่เห็นด้วย</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D7[6]"
                className="form-check-input"
                onClick={() => ChangeDomain7(6, -1)}
              />
              <label className="form-check-label">
                ไม่เห็นด้วยเป็นบ่างอย่าง
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D7[6]"
                className="form-check-input"
                onClick={() => ChangeDomain7(6, 0)}
              />
              <label className="form-check-label">เฉยๆ</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D7[6]"
                className="form-check-input"
                onClick={() => ChangeDomain7(6, 1)}
              />
              <label className="form-check-label">เห็นด้วยเป็นบางอย่าง</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D7[6]"
                className="form-check-input"
                onClick={() => ChangeDomain7(6, 2)}
              />
              <label className="form-check-label">เห็นด้วย</label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D7[6]"
                className="form-check-input"
                onClick={() => ChangeDomain7(6, 3)}
              />
              <label className="form-check-label">เห็นด้วยอย่างยิ่ง</label>
            </div>
          </div>
          <div ref={myRefdomain8}>
            <h4 className="mt-5">โดเมนที่ 8</h4>
          </div>
          <div>
            <p>
              1.
              เมื่อคุณไปซื้อของที่ร้านสะดวกซื้อแล้วได้รับเงินทอนมาเกินจากจำนวนจริง
              คุณจะ
            </p>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D8[1]"
                className="form-check-input"
                onClick={() => ChangeDomain8(1, 1, 0)}
              />
              <label className="form-check-label">
                a) นำเงินส่วนเกินใส่กล่องทิปพนักงาน
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D8[1]"
                className="form-check-input"
                onClick={() => ChangeDomain8(1, 2, 1)}
              />
              <label className="form-check-label">
                b) แจ้งพนักงานแคชเชียร์ถึงความผิดพลาด
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D8[1]"
                className="form-check-input"
                onClick={() => ChangeDomain8(1, 3, 0)}
              />
              <label className="form-check-label">
                c)
                นำเงินใส่ในกล่องบริจาคให้มูลนิธิที่ตั้งอยู่หน้าร้านเพื่อทำประโยชน์ให้คนอื่นต่อไป
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D8[1]"
                className="form-check-input"
                onClick={() => ChangeDomain8(1, 4, 0)}
              />
              <label className="form-check-label">
                d)
                เก็บเงินไว้เพราะต้องการสอนบทเรียนให้กับพนักงานเพื่อให้ทำงานอย่างรอบคอบมากขึ้น
              </label>
            </div>
          </div>
          <div>
            <p>
              2. บุรุษไปรษณีย์ส่งพัสดุที่จ่าหน้าซองถึงคนอื่นให้คุณ
              เมื่อคุณเห็นพัสดุนั้นคุณจะทำอย่างไร
            </p>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D8[2]"
                className="form-check-input"
                onClick={() => ChangeDomain8(2, 1, 0)}
              />
              <label className="form-check-label">
                a) เปิดดูข้างในว่ามีอะไร
                เพื่อเช็คว่าเป็นของที่ตัวเองสั่งไว้หรือเปล่า
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D8[2]"
                className="form-check-input"
                onClick={() => ChangeDomain8(2, 2, 0)}
              />
              <label className="form-check-label">
                b) วางทิ้งไว้ที่เดิม เพื่อที่บุรุษไปรษณีย์จะได้มาเอาคืนไป
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D8[2]"
                className="form-check-input"
                onClick={() => ChangeDomain8(2, 3, 0)}
              />
              <label className="form-check-label">
                c) ถามเพื่อนบ้านระแวกของคุณว่ามีใครสั่งพัสดุไหม
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D8[2]"
                className="form-check-input"
                onClick={() => ChangeDomain8(2, 4, 1)}
              />
              <label className="form-check-label">
                d) ขับรถไปนำส่งคืนที่ไปรษณีย์เขตบ้านคุณ
              </label>
            </div>
          </div>
          <div>
            <p>
              3. เมื่อคุณทำงานผิดพลาด และหัวหน้าของคุณตำหนิผลงานของทีม
              กลางที่ประชุม คุณจะ...
            </p>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D8[3]"
                className="form-check-input"
                onClick={() => ChangeDomain8(3, 1, 0)}
              />
              <label className="form-check-label">
                a) ขอโทษทีมของคุณที่คุณทำผิดพลาด
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D8[3]"
                className="form-check-input"
                onClick={() => ChangeDomain8(3, 2, 0)}
              />
              <label className="form-check-label">
                b) แก้ไขงานนั้นทันที และนำเสนอหัวหน้าใหม่
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D8[3]"
                className="form-check-input"
                onClick={() => ChangeDomain8(3, 3, 1)}
              />
              <label className="form-check-label">
                c) ยอมรับและชี้แจงเหตุผลว่าทำไมคุณถึงทำผิดพลาด ณ ที่ตรงนั้น
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D8[3]"
                className="form-check-input"
                onClick={() => ChangeDomain8(3, 4, 0)}
              />
              <label className="form-check-label">
                d) จดบันทึกถึงข้อผิดพลาด และนำไปพัฒนาตนเองในงานหน้า
              </label>
            </div>
          </div>
          <div>
            <p>
              4. เมื่อคุณได้ยิน คนที่ทำงานนินทาและปล่อยข่าวลือที่ไม่จริงออกไป
              คุณจะ
            </p>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D8[4]"
                className="form-check-input"
                onClick={() => ChangeDomain8(4, 1, 0)}
              />
              <label className="form-check-label">
                a) เงียบฟังเอาไว้ และนำไปบอกเจ้าทุกข์ว่ามีคนนินทาเขาอยู่
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D8[4]"
                className="form-check-input"
                onClick={() => ChangeDomain8(4, 2, 0)}
              />
              <label className="form-check-label">
                b) แจ้งหัวหน้าของคุณว่ามีเหตุการณ์แบบนี้เกิดขึ้นในบริษัท
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D8[4]"
                className="form-check-input"
                onClick={() => ChangeDomain8(4, 3, 1)}
              />
              <label className="form-check-label">
                c) บอกคนที่นินทาและปล่อยข่าวลือว่า สิ่งที่เขาทำไม่ถูก
                ไม่ควรทำแบบนั้น
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D8[4]"
                className="form-check-input"
                onClick={() => ChangeDomain8(4, 4, 0)}
              />
              <label className="form-check-label">
                d)
                เก็บเงียบเอาไว้เพราะถือว่าไม่ใช่เรื่องที่ตัวเองจะเข้าไปเกี่ยวข้อง
                เอาเวลาไปโฟกัสกับเรื่องงานของตัวเองดีกว่า
              </label>
            </div>
          </div>
          <div>
            <p>
              5. ถ้าหากว่าแฟนของคุณนอกใจไปคบคนอื่น
              หลังจากนั้นคุณเกิดรู้เข้าว่าแฟนเก่าของคุณโดนสวมเขา คุณจะ
            </p>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D8[5]"
                className="form-check-input"
                onClick={() => ChangeDomain8(5, 1, 0)}
              />
              <label className="form-check-label">
                a) คิดว่ามันเป็นเรื่องของผลกรรม ไม่สนใจและเดินหน้าใช้ชีวิตต่อ
                ไม่จมปลักกับความหลัง
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D8[5]"
                className="form-check-input"
                onClick={() => ChangeDomain8(5, 2, 1)}
              />
              <label className="form-check-label">
                b) ทักไปบอกแฟนเก่าว่า
                เขาโดนแฟนคนใหม่สวมเขาให้อยู่เพราะคิดว่าสิ่งที่เขาโดน
                มันเป็นสิ่งที่ไม่ถูกต้อง
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D8[5]"
                className="form-check-input"
                onClick={() => ChangeDomain8(5, 3, 0)}
              />
              <label className="form-check-label">
                c) ไปต่อว่าแฟนใหม่คนนั้น เรื่องที่สวมเขาให้แฟนเก่าคุณ
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D8[5]"
                className="form-check-input"
                onClick={() => ChangeDomain8(5, 4, 0)}
              />
              <label className="form-check-label">
                d) นำเรื่องนี้มาปรึกษาเพื่อนของคุณก่อนว่าควรทำอย่างไรต่อ
              </label>
            </div>
          </div>
          <div>
            <p>
              6. เมื่อเพื่อนของน้ำหนักขึ้นจนเห็นได้ชัด และถามคุณว่า
              เธออ้วนขึ้นไหม คุณตอบว่า
            </p>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D8[6]"
                className="form-check-input"
                onClick={() => ChangeDomain8(6, 1, 0)}
              />
              <label className="form-check-label">
                a) ‘ไม่อ้วนหรอก’ เพื่อรักษาน้ำใจของเพื่อนคุณ
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D8[6]"
                className="form-check-input"
                onClick={() => ChangeDomain8(6, 2, 0)}
              />
              <label className="form-check-label">
                b) เปลี่ยนเรื่องคุยอย่างมีไหวพริบ
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D8[6]"
                className="form-check-input"
                onClick={() => ChangeDomain8(6, 3, 1)}
              />
              <label className="form-check-label">
                c) บอกไปตามตรงว่า ‘ใช่’
                เพื่อที่เพื่อนจะได้รับรู้ถึงความจริงและนำไปปรับปรุง
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D8[6]"
                className="form-check-input"
                onClick={() => ChangeDomain8(6, 4, 0)}
              />
              <label className="form-check-label">
                d) เลี่ยงที่จะตอบโดยชวนเพื่อนไปออกกำลังกาย
                หรือสมัครฟิตเนสด้วยกันแทน
              </label>
            </div>
          </div>
          <div ref={myRefdomain9}>
            <h4 className="mt-5">โดเมนที่ 9</h4>
          </div>
          <div>
            <p>1. คุณคิดว่าอะไรคือสิ่งที่คุ้มค่าที่สุดเกี่ยวกับงานของคุณ?</p>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D9[1]"
                className="form-check-input"
                onClick={() => ChangeDomain9(1, 1, "a")}
              />
              <label className="form-check-label">
                a) ฉันเรียนรู้และเติบโตตลอดเวลา
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D9[1]"
                className="form-check-input"
                onClick={() => ChangeDomain9(1, 2, "c")}
              />
              <label className="form-check-label">
                b) ได้เป็นแหล่งพัฒนาด้านความคิดสร้างสรรค์ต่างๆ
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D9[1]"
                className="form-check-input"
                onClick={() => ChangeDomain9(1, 3, "s")}
              />
              <label className="form-check-label">
                c) ได้สร้างมิตรภาพที่ใกล้ชิดที่สุดกับเพื่อนร่วมงาน
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D9[1]"
                className="form-check-input"
                onClick={() => ChangeDomain9(1, 4, "g")}
              />
              <label className="form-check-label">
                d)
                ฉันรู้สึกราวกับว่ากำลังสร้างความแตกต่างให้กับคนที่ต้องการความช่วยเหลือ
              </label>
            </div>
          </div>
          <div>
            <p>2. หากลูกถามคุณว่าอะไรสำคัญในชีวิตคุณจะตอบว่า …</p>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D9[2]"
                className="form-check-input"
                onClick={() => ChangeDomain9(2, 1, "g")}
              />
              <label className="form-check-label">
                a) การทำประโยชน์ให้คนอื่น
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D9[2]"
                className="form-check-input"
                onClick={() => ChangeDomain9(2, 2, "s")}
              />
              <label className="form-check-label">
                b) อย่าปล่อยปละละเลยคนที่เรารัก
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D9[2]"
                className="form-check-input"
                onClick={() => ChangeDomain9(2, 3, "a")}
              />
              <label className="form-check-label">
                c) เปิดใจเรียนรู้สิ่งใหม่ๆ
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D9[2]"
                className="form-check-input"
                onClick={() => ChangeDomain9(2, 4, "c")}
              />
              <label className="form-check-label">
                d) สนุกสนานไปกับความสวยงามของโลกของนี้
              </label>
            </div>
          </div>
          <div>
            <p>3. วลีใดต่อไปนี้ตรงกับคติส่วนตัวของคุณมากที่สุด</p>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D9[3]"
                className="form-check-input"
                onClick={() => ChangeDomain9(3, 1, "s")}
              />
              <label className="form-check-label">
                a) สร้างความสัมพันธ์ที่มีความหมายกับผู้อื่น
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D9[3]"
                className="form-check-input"
                onClick={() => ChangeDomain9(3, 2, "a")}
              />
              <label className="form-check-label">
                b) พัฒนาไปถึงศักยภาพของคุณอย่างสูงสุด
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D9[3]"
                className="form-check-input"
                onClick={() => ChangeDomain9(3, 3, "c")}
              />
              <label className="form-check-label">
                c) ชื่นชมความสวยงามของโลกนี้
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D9[3]"
                className="form-check-input"
                onClick={() => ChangeDomain9(3, 4, "g")}
              />
              <label className="form-check-label">
                d) ส่งต่อเจตนาดี เอื้ออารีย์ให้กับผู้อื่น
              </label>
            </div>
          </div>
          <div>
            <p>
              4. ลองจินตนาการถึงการใช้เวลาตอนเช้าในการทำงาน
              สิ่งใดที่จะทำให้คุณพอใจมากที่สุด
            </p>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D9[4]"
                className="form-check-input"
                onClick={() => ChangeDomain9(4, 1, "c")}
              />
              <label className="form-check-label">
                a) ระดมไอเดียใหม่ๆ กับทีมของคุณ
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D9[4]"
                className="form-check-input"
                onClick={() => ChangeDomain9(4, 2, "a")}
              />
              <label className="form-check-label">
                b) เข้าไปฟังการบรรยายเพื่อสร้างแรงบันดาลใจในการทำงาน
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D9[4]"
                className="form-check-input"
                onClick={() => ChangeDomain9(4, 3, "g")}
              />
              <label className="form-check-label">
                c) ช่วยเหลือลูกค้าในการแก้ปัญหาในระยะยาวในปัญหาของลูกค้า
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D9[4]"
                className="form-check-input"
                onClick={() => ChangeDomain9(4, 4, "s")}
              />
              <label className="form-check-label">
                d) เข้าไปมีส่วนร่วมในแบบฝึกการทำงานเป็นทีม
              </label>
            </div>
          </div>
          <div>
            <p>
              5. หากคุณได้ยินเพื่อนร่วมงานนินทาคุณ
              ความคิดเห็นใดที่ทำให้คุณรู้สึกแย่มากที่สุด
            </p>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D9[5]"
                className="form-check-input"
                onClick={() => ChangeDomain9(5, 1, "c")}
              />
              <label className="form-check-label">
                a) ‘เธอช่างเป็นคนที่ไม่มีจินตนาการเอาเสียเลย’
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D9[5]"
                className="form-check-input"
                onClick={() => ChangeDomain9(5, 2, "s")}
              />
              <label className="form-check-label">
                b) ‘เธอไม่ได้ป็อปปูล่าร์อย่างที่คิดหรอก’
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D9[5]"
                className="form-check-input"
                onClick={() => ChangeDomain9(5, 3, "a")}
              />
              <label className="form-check-label">
                c) ‘เธอเหมือนไดโนเสาร์ที่ติดอยู่ในอดีตนั่นแหละ’
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D9[5]"
                className="form-check-input"
                onClick={() => ChangeDomain9(5, 4, "g")}
              />
              <label className="form-check-label">
                d) ‘เธอตั้งใจที่แต่จะเอาชนะพนักงานดีเด่นคนนั้น’
              </label>
            </div>
          </div>
          <div>
            <p>6. เมื่อมองย้อนกลับไปในชีวิตของคุณคุณจะหวังเห็นอะไร?</p>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D9[6]"
                className="form-check-input"
                onClick={() => ChangeDomain9(6, 1, "s")}
              />
              <label className="form-check-label">
                a)
                ความสัมพันธ์ที่ยั่งยืนและมีความหมายกับเพื่อนร่วมงานและครอบครัว
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D9[6]"
                className="form-check-input"
                onClick={() => ChangeDomain9(6, 2, "c")}
              />
              <label className="form-check-label">
                b)
                คุณได้สำรวจตนเองและพัฒนาความสามารถในด้านความคิดสร้างสรรค์ของคุณอย่างเต็มที่
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D9[6]"
                className="form-check-input"
                onClick={() => ChangeDomain9(6, 3, "g")}
              />
              <label className="form-check-label">
                c)
                คุณได้ทุ่มเทและก้าวไปอีกขั้นเพื่อสร้างความแตกต่างให้กับผู้อื่น
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D9[6]"
                className="form-check-input"
                onClick={() => ChangeDomain9(6, 4, "a")}
              />
              <label className="form-check-label">
                d) คุณไม่เคยหยุดเรียนรู้พัฒนาและเติบโตขึ้น
              </label>
            </div>
          </div>
          <div ref={myRefdomain10}>
            <h4 className="mt-5">โดเมนที่ 10</h4>
          </div>
          ถ้าหากคุณตื่นขึ้นมาในถ้ำใต้ดินและสูญเสียความทรงจำทั้งหมดว่าคุณเป็นใคร
          มาอยู่ที่นี่ได้อย่างไร
          ตรงหน้ามีตัวอักษรบนผนังถ้ำเขียนไว้ว่าที่นี่คือใต้พิ้นโลก
          และทางที่ออกจากที่นี่ไปสู่ผิวโลกต้องคุณจะต้องเดินทางผ่านอุโมงที่เหมือนเขาวงกตนี้
          <div>
            <p>
              1. คุณพบกระเป๋าที่อยู่บนพื้น ในนั้นมีดาบ
              และเครื่องมือสื่อสารชนิดหนึ่ง เมื่อคุณเริ่มเดิน
              ทางออกจากถ้ำเงามืดปรากฎขึ้นในอุโมงข้างหน้าของคุณ
              คุณเห็นตัวตุ่นยักษ์ และสบตากับมันที่กำลังขวางทางเดินคุณอยู่
              คุณจะ...
            </p>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D10[1]"
                className="form-check-input"
                onClick={() => ChangeDomain10(1, 1, "E")}
              />
              <label className="form-check-label">
                a) ใช้อุปกรณ์สื่อสารทักทายและแนะนำตัว
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D10[1]"
                className="form-check-input"
                onClick={() => ChangeDomain10(1, 2, "C")}
              />
              <label className="form-check-label">
                b) ค่อยๆประเมินสถาณการณ์จากสิ่งรอบข้างและเคลื่อนไปดู
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D10[1]"
                className="form-check-input"
                onClick={() => ChangeDomain10(1, 3, "O")}
              />
              <label className="form-check-label">
                c) เดินเข้าไปดูตัวตุ่นนั้นว่าคืออะไรกันแน่
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D10[1]"
                className="form-check-input"
                onClick={() => ChangeDomain10(1, 4, "A")}
              />
              <label className="form-check-label">
                d) ตัดสินใจว่าตัวตุ่นน่าจะเชื่อถือได้ และถามทางกับมัน
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D10[1]"
                className="form-check-input"
                onClick={() => ChangeDomain10(1, 5, "N")}
              />
              <label className="form-check-label">
                e) กรีดร้องขอความช่วยเหลือ และวิ่งหนี
              </label>
            </div>
          </div>
          <div>
            <p>
              2. เมื่อคุณเดินทางต่อไปตามทาง ทันใดนั้นก็มีเสียงดังสนั่นออก
              มาจากพื้น
              สิ่งมีชีวิตที่คล้ายกับหนอนยักษ์ก็โผล่ขึ้นมาจากพื้นในอุโมงค์
              สิ่งมีชีวิตนั้นหยุดขุดดินเมื่อเห็นคุณเข้า คุณจะ...
            </p>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D10[2]"
                className="form-check-input"
                onClick={() => ChangeDomain10(2, 1, "O")}
              />
              <label className="form-check-label">
                a) คุณมองดูเหตุการณ์
                ด้วยความตื่นเต้นและรอดูว่าสิ่งมีชีวิตนั้นจะทำอะไรต่อไป
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D10[2]"
                className="form-check-input"
                onClick={() => ChangeDomain10(2, 2, "E")}
              />
              <label className="form-check-label">
                b)
                คุณอุทานออกมาด้วยความประหลาดใจและแนะนำตัวเองกับสิ่งมีชีวิตใหม่ตัวนี้
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D10[2]"
                className="form-check-input"
                onClick={() => ChangeDomain10(2, 3, "A")}
              />
              <label className="form-check-label">
                c) คุณกล่าวขอโทษที่ขัดขวางการขุดของมัน
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D10[2]"
                className="form-check-input"
                onClick={() => ChangeDomain10(2, 4, "C")}
              />
              <label className="form-check-label">
                d)
                คุณพยายามประเมินว่าสิ่งมีชีวิตนี้มาโผล่ที่นี่ได้ยังไงเพื่อหาทางออกอื่น
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D10[2]"
                className="form-check-input"
                onClick={() => ChangeDomain10(2, 5, "N")}
              />
              <label className="form-check-label">
                e) คุณคิดว่า“ฉันคงไม่มีชีวิตรอดออกไปจากที่นี่แน่ๆ”
              </label>
            </div>
          </div>
          <div>
            <p>
              3. หลังจากคุณโดนตัวหนอนทำร้ายและ ใช้เวลาหนีจนมืด
              คุณเหนื่อยล้าเต็มที่ พร้อมที่จะหา ที่นอน
              แต่ปลายทางอุโมงแห่งใหม่คุณเห็นดวงตาสีเหลืองโผล่ออกมาจากความมืด
              เมื่อเข้าไปใกล้คุณก็พบกับแมวที่ดูจะปกติที่สุดเท่าที่คุณเจอมาตลอดทาง
              แมวตัวนั้นมองคุณและหันหัวกลับไปนอนที่เดิม ก่อนล้มตัวลงนอน
            </p>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D10[3]"
                className="form-check-input"
                onClick={() => ChangeDomain10(3, 1, "C")}
              />
              <label className="form-check-label">
                a) “ฉันคิดว่าคืนนี้เรามานอนด้วยกันตรงนี้เถอะ และผลัดกันเฝ้ากะ
                เผื่อมีสัตว์ร้ายยามดึก
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D10[3]"
                className="form-check-input"
                onClick={() => ChangeDomain10(3, 2, "A")}
              />
              <label className="form-check-label">
                b)
                “ถ้าคุณช่วยให้ฉันอยู่ที่นี่ในคืนนี้ฉันรู้สึกว่าเราสามารถช่วยเหลือกันได้คุณอาจรู้ทางของอุโมงนี้
                ส่วนฉันมีดาบสำหรับใช้ปกป้องเราสองคน”
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D10[3]"
                className="form-check-input"
                onClick={() => ChangeDomain10(3, 3, "N")}
              />
              <label className="form-check-label">
                c) ฉันเกือบโดนฆ่าตอนที่เจอกับพวกสัตว์ก่อนหน้านี้
                ฉันต้องการที่ซุกหัวนอนจริงๆ ช่วยฉันเถอะ
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D10[3]"
                className="form-check-input"
                onClick={() => ChangeDomain10(3, 4, "E")}
              />
              <label className="form-check-label">
                d) คุณเดินไปนั่งลง และพยายามเข้าไปคุยและสื่อสารกับแมวตัวนั้น
                หลังจากแมวตัวนั้นไม่ตอบ คุณผล็อยหลับไป
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D10[3]"
                className="form-check-input"
                onClick={() => ChangeDomain10(3, 5, "O")}
              />
              <label className="form-check-label">
                e) คุณรวบรวมสิ่งของของคุณเพื่อตรวจสอบอุโมงแห่งใหม่นี้
              </label>
            </div>
          </div>
          <div>
            <p>
              4. เมื่อคุณตืนขึ้นมีสัตว์ชนิดอื่นปรากฏตัวขึ้นกับแมวตัวนั้น พวก
              มันนำทางคุณไปที่รูหนอนที่คุณเคยเห็นหนอนขุดแล้วพูดว่า
              ฉันสังเกตได้ว่าคุณกำลังหลงทางอยู่ นี่คือทางออกจากที่แห่งนี้
            </p>
            <div className="form-check  ml-5">
              <input
                type="radio"
                name="D10[4]"
                className="form-check-input"
                onClick={() => ChangeDomain10(4, 1, "O")}
              />
              <label className="form-check-label">
                a)
                คุณยังคงรู้สึกสงสัยและอยากหาคำตอบกับสิ่งลึกลับที่เกิดขึ้นที่นี่
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                name="D10[4]"
                type="radio"
                className="form-check-input"
                onClick={() => ChangeDomain10(4, 2, "E")}
              />
              <label className="form-check-label">
                b) ขณะที่คุณยังอยากจะคุยกับแมวตัวนี้
                คุณก็อยากคุยและทำความรู้จักกับเพื่อนของมันด้วย
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                name="D10[4]"
                type="radio"
                className="form-check-input"
                onClick={() => ChangeDomain10(4, 3, "A")}
              />
              <label className="form-check-label">
                c) คุณเชื่อว่าแมวตัวนี้หวังดีกับคุณ
                คุณเตรียมตัวที่จะลงไปในรูหนอนนี้ทันที
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                name="D10[4]"
                type="radio"
                className="form-check-input"
                onClick={() => ChangeDomain10(4, 4, "N")}
              />
              <label className="form-check-label">
                d) คุณลังเลใจที่จะเชื่อใจแมวตัวนี้
                และพยายามถามว่ารูหนอนนี้อันตรายรึเปล่า
              </label>
            </div>
            <div className="form-check  ml-5">
              <input
                name="D10[4]"
                type="radio"
                className="form-check-input"
                onClick={() => ChangeDomain10(4, 5, "C")}
              />
              <label className="form-check-label">
                e) คุณพยักหน้าตอบรับ
                เช็คความเรียบร้อยของสิ่งของของคุณให้เรียบร้อยเพื่อดูว่าคุณไม่ลืมอะไร
              </label>
            </div>
          </div>
          <button
            type="button"
            className="btn btn-primary mt-5"
            onClick={() => SendAnswer()}
          >
            ส่งคำตอบ
          </button>
        </h4>
      </div>
    </>
  );
}
