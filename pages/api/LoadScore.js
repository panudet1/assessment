import dbConnect from "../../utils/dbConnect";
import Score from "../../models/Score";
dbConnect();

export default async (req, res) => {
  const { method } = req;
  switch (method) {
    case "POST":
      try {
        const score = await Score.findById(req.body.id).exec();
        res.status(200).json({ success: true, data: score });
      } catch (error) {
        res.status(400).json({ success: false });
      }
      break;
    case "GET":
      break;
    default:
      res.status(400).json({ success: false });
      break;
  }
};
