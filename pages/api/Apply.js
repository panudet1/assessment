import dbConnect from "../../utils/dbConnect";
import Score from "../../models/Score";
dbConnect();

export default async (req, res) => {
  const { method } = req;
  switch (method) {
    case "POST":
      try {
        var item = {
          user_id: req.body.user_id,
          domain1: req.body.domain1,
          domain2: req.body.domain2,
          domain3: req.body.domain3,
          domain4: req.body.domain4,
          domain5: req.body.domain5,
          domain6: req.body.domain6,
          domain7: req.body.domain7,
          domain8: req.body.domain8,
          domain9: req.body.domain9,
          domain10: req.body.domain10,
          series_multiple: req.body.series_multiple,
          series_scale_likert: req.body.series_scale_likert,
          series_multiple_extra: req.body.series_multiple_extra,
          series_multiple_ocean: req.body.series_multiple_ocean,
          date: new Date(),
        };
        const score = await Score.create(item);
        // console.log(score._id);
        res.status(200).json({ success: true, data: score._id });
      } catch (error) {
        res.status(400).json({ success: false });
      }
      break;
    case "GET":
      break;
    default:
      res.status(400).json({ success: false });
      break;
  }
};
