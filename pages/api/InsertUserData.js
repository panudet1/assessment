import dbConnect from "../../utils/dbConnect";
import User from "../../models/User";
dbConnect();

export default async (req, res) => {
  const { method } = req;
  switch (method) {
    case "POST":
      try {
        var item = {
          email: req.body.email,
          first_name: req.body.first_name,
          last_name: req.body.last_name,
          id: req.body.id,
          name: req.body.name,
          picture: req.body.picture,
          short_name: req.body.short_name,
          date: new Date(),
        };
        await User.create(item);
        res.status(200).json({ success: true, data: req.body.id });
      } catch (error) {
        res.status(400).json({ success: false });
      }
      break;
    case "GET":
      break;
    default:
      res.status(400).json({ success: false });
      break;
  }
};
