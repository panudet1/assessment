import React, { Component } from "react";
import dynamic from "next/dynamic";
const Chart = dynamic(() => import("react-apexcharts"), { ssr: false });
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      series_multiple: [
        {
          name: "Series 1",
          data: [6, 3, 2, 1],
        },
      ],
      options_multiple: {
        chart: {
          height: 350,
          type: "radar",
        },
        dataLabels: {
          enabled: true,
        },
        plotOptions: {
          radar: {
            size: 140,
            polygons: {
              strokeColors: "#e9e9e9",
              fill: {
                colors: ["#f8f8f8", "#fff"],
              },
            },
          },
        },
        title: {
          text: "Multiple choice",
        },
        colors: ["#FF4560"],
        markers: {
          size: 4,
          colors: ["#fff"],
          strokeColor: "#FF4560",
          strokeWidth: 2,
        },
        tooltip: {
          y: {
            formatter: function (val) {
              return val;
            },
          },
        },
        xaxis: {
          categories: [
            "Logic",
            "Giving ",
            "Competency & role mention",
            "Integrity & ethic",
          ],
        },
        yaxis: {
          tickAmount: 10,
          labels: {
            formatter: function (val, i) {
              return "";
            },
          },
        },
      },
      series_scale_likert: [
        {
          name: "Series 1",
          data: [10, 20, 30, 40],
        },
      ],
      options_scale_likert: {
        chart: {
          height: 350,
          type: "radar",
        },
        dataLabels: {
          enabled: true,
        },
        plotOptions: {
          radar: {
            size: 140,
            polygons: {
              strokeColors: "#e9e9e9",
              fill: {
                colors: ["#f8f8f8", "#fff"],
              },
            },
          },
        },
        title: {
          text: "Scale-likert",
        },
        colors: ["#FF4560"],
        markers: {
          size: 4,
          colors: ["#fff"],
          strokeColor: "#FF4560",
          strokeWidth: 2,
        },
        tooltip: {
          y: {
            formatter: function (val) {
              return val;
            },
          },
        },
        xaxis: {
          categories: ["Teamwork", "Communication", "Passion/ambition", "Grit"],
        },
        yaxis: {
          tickAmount: 10,
          labels: {
            formatter: function (val, i) {
              return "";
            },
          },
        },
      },
      series_multiple_extra: [
        {
          name: "Series 1",
          data: [10, 20, 30, 40],
        },
      ],
      options_multiple_extra: {
        chart: {
          height: 350,
          type: "radar",
        },
        dataLabels: {
          enabled: true,
        },
        plotOptions: {
          radar: {
            size: 140,
            polygons: {
              strokeColors: "#e9e9e9",
              fill: {
                colors: ["#f8f8f8", "#fff"],
              },
            },
          },
        },
        title: {
          text: "Multiple extra",
        },
        colors: ["#FF4560"],
        markers: {
          size: 4,
          colors: ["#fff"],
          strokeColor: "#FF4560",
          strokeWidth: 2,
        },
        tooltip: {
          y: {
            formatter: function (val) {
              return val;
            },
          },
        },
        xaxis: {
          categories: ["Ambition", "Creativity", "Social", "Giver"],
        },
        yaxis: {
          tickAmount: 10,
          labels: {
            formatter: function (val, i) {
              return "";
            },
          },
        },
      },
      series_multiple_ocean: [
        {
          name: "Series 1",
          data: [20, 100, 40, 30, 50],
        },
      ],
      options_multiple_ocean: {
        chart: {
          height: 350,
          type: "radar",
        },
        dataLabels: {
          enabled: true,
        },
        plotOptions: {
          radar: {
            size: 140,
            polygons: {
              strokeColors: "#e9e9e9",
              fill: {
                colors: ["#f8f8f8", "#fff"],
              },
            },
          },
        },
        title: {
          text: "OCEAN",
        },
        colors: ["#FF4560"],
        markers: {
          size: 4,
          colors: ["#fff"],
          strokeColor: "#FF4560",
          strokeWidth: 2,
        },
        tooltip: {
          y: {
            formatter: function (val) {
              return val;
            },
          },
        },
        xaxis: {
          categories: [
            "openness to experience",
            "conscientiousness",
            "extraversion",
            "agreeableness",
            "neuroticism",
          ],
        },
        yaxis: {
          tickAmount: 10,
          labels: {
            formatter: function (val, i) {
              return "";
            },
          },
        },
      },
    };
  }

  async componentDidMount() {
    var id = await localStorage.getItem("id");

    // var id = "600158b75003c9509cb281de";
    var data = {
      id: id,
    };
    this.methodName(data);
  }
  methodName = async (data) => {
    console.log(data);
    await fetch("/api/LoadScore", {
      method: "POST", // or 'PUT'
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer my-token",
        "My-Custom-Header": "foobar",
      },
      body: JSON.stringify(data),
    })
      .then((response) => response.json())
      .then((data) => {
        // console.log(data.data);
        this.setState({
          series_multiple: data.data.series_multiple,
          series_scale_likert: data.data.series_scale_likert,
          series_multiple_extra: data.data.series_multiple_extra,
          series_multiple_ocean: data.data.series_multiple_ocean,
        });
      })

      .catch((error) => {
        console.error("Error:", error);
      });
  };

  render() {
    return (
      <>
        <div className="container mt-5">
          <div className="card ">
            <Chart
              className="d-flex justify-content-center"
              options={this.state.options_multiple}
              series={this.state.series_multiple}
              type="radar"
              height={350}
              width={600}
            />
          </div>
        </div>
        <div className="container mt-5">
          <div className="card ">
            <Chart
              className="d-flex justify-content-center"
              options={this.state.options_scale_likert}
              series={this.state.series_scale_likert}
              type="radar"
              height={350}
              width={600}
            />
          </div>
        </div>
        <div className="container mt-5">
          <div className="card ">
            <Chart
              className="d-flex justify-content-center"
              options={this.state.options_multiple_extra}
              series={this.state.series_multiple_extra}
              type="radar"
              height={350}
              width={600}
            />
          </div>
        </div>
        <div className="container mt-5">
          <div className="card ">
            <Chart
              className="d-flex justify-content-center"
              options={this.state.options_multiple_ocean}
              series={this.state.series_multiple_ocean}
              type="radar"
              height={350}
              width={600}
            />
          </div>
        </div>
      </>
    );
  }
}

export default App;
