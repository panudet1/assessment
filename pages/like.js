import React, { Component } from "react";
import { FacebookProvider, LoginButton } from "react-facebook";
import { useRouter } from "next/router";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  handleResponse = (data) => {
    const router = useRouter();
    // console.log(data.profile);

    var data = {
      email: data.profile.email,
      first_name: data.profile.first_name,
      last_name: data.profile.last_name,
      id: data.profile.id,
      name: data.profile.name,
      picture: data.profile.picture,
      short_name: data.profile.short_name,
    };
    fetch("/api/InsertUserData", {
      method: "POST", // or 'PUT'
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer my-token",
        "My-Custom-Header": "foobar",
      },
      body: JSON.stringify(data),
    })
      .then((response) => response.json())
      .then((data) => {
        localStorage.setItem("user_id", data.data);
        console.log(data.data);
        return router.push("/test");
      })

      .catch((error) => {
        console.error("Error:", error);
      });
  };
  render() {
    return (
      <>
        <div
          style={{ height: "100vh" }}
          className="d-flex justify-content-center align-items-center"
        >
          <FacebookProvider appId="814874269026743">
            <LoginButton
              className="btn btn-primary btn-lg"
              scope="email"
              onCompleted={this.handleResponse}
            >
              ดำเนินการต่อด้วย Facebook
            </LoginButton>
          </FacebookProvider>
        </div>
      </>
    );
  }
}

export default App;
