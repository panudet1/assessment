import React, { useState, useEffect, useRef } from "react";
import { useRouter } from "next/router";
import { FacebookProvider, LoginButton } from "react-facebook";
import Modal from "react-modal";
const customStyles2 = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
  },
};
export default function Home() {
  const router = useRouter();
  const [isloading, Isloading] = useState(false);
  const handleResponse = (data) => {
    Isloading(true);
    var data = {
      email: data.profile.email,
      first_name: data.profile.first_name,
      last_name: data.profile.last_name,
      id: data.profile.id,
      name: data.profile.name,
      picture: data.profile.picture,
      short_name: data.profile.short_name,
    };
    fetch("/api/InsertUserData", {
      method: "POST", // or 'PUT'
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer my-token",
        "My-Custom-Header": "foobar",
      },
      body: JSON.stringify(data),
    })
      .then((response) => response.json())
      .then((data) => {
        localStorage.setItem("user_id", data.data);
        console.log(data.data);
        Isloading(false);
        return router.push("/test");
      })
      .catch((error) => {
        console.error("Error:", error);
      });
  };

  return (
    <>
      <div
        style={{ height: "100vh" }}
        className="d-flex justify-content-center align-items-center"
      >
        <Modal style={customStyles2} isOpen={isloading}>
          <div class="spinner-border text-success" role="status">
            <span class="sr-only">Loading...</span>
          </div>
        </Modal>
        <FacebookProvider appId="814874269026743">
          <LoginButton
            className="btn btn-primary btn-lg"
            scope="email"
            onCompleted={handleResponse}
          >
            ดำเนินการต่อด้วย Facebook
          </LoginButton>
        </FacebookProvider>
      </div>
    </>
  );
}
