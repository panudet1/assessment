import React from "react";
import { signIn, signOut, useSession } from "next-auth/client";

export default function myComponent() {
  const [session, loading] = useSession();

  console.log(session);
  return (
    <>
      {!session && (
        <>
          Not signed in <br />
          {/* <button
            onClick={signIn("facebook", {
              callbackUrl: "http://localhost:3000/",
            })}
          >
            Sign in
          </button> */}
          <button onClick={signIn}>Sign in</button>
        </>
      )}
      {session && (
        <>
          Signed in as {session.user.email} <br />
          <button onClick={signOut}>Sign out</button>
        </>
      )}
    </>
  );
}
