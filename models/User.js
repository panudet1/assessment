const mongoose = require("mongoose");
const UserSchema = new mongoose.Schema({
  email: {
    type: String,
  },
  first_name: {
    type: String,
  },
  id: {
    type: String,
  },
  last_name: {
    type: String,
  },
  name: {
    type: String,
  },
  picture: {
    type: Array,
  },
  short_name: {
    type: String,
  },
  date: {
    type: Date,
    maxlength: [300, "Title cannot be more than 40 characters"],
  },
});
module.exports = mongoose.model.User || mongoose.model("User", UserSchema);
