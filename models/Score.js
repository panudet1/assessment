const mongoose = require("mongoose");
const ScoreSchema = new mongoose.Schema({
  user_id: {
    type: String,
  },
  domain1: {
    type: Array,
  },
  domain2: {
    type: Array,
  },
  domain3: {
    type: Array,
  },
  domain4: {
    type: Array,
  },
  domain5: {
    type: Array,
  },
  domain6: {
    type: Array,
  },
  domain7: {
    type: Array,
  },
  domain8: {
    type: Array,
  },
  domain9: {
    type: Array,
  },
  domain10: {
    type: Array,
  },
  series_multiple: {
    type: Array,
  },
  series_scale_likert: {
    type: Array,
  },
  series_multiple_extra: {
    type: Array,
  },
  series_multiple_ocean: {
    type: Array,
  },

  date: {
    type: Date,
    maxlength: [300, "Title cannot be more than 40 characters"],
  },
});
module.exports = mongoose.model.Score || mongoose.model("Score", ScoreSchema);
